package Project.GitLab_Sample;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

	public class CloudBLM_Smoke_Project {

		public static WebDriver driver;
		public static File src;
		public static ExtentTest test;
		public static ExtentReports report;
		public static FileInputStream fis;
		public static HSSFSheet sh1, sh2;
		public static HSSFWorkbook wb;
		public static Template templates;
		public static VelocityContext context;
		public static String Build;
		static long overallstartTime = 0;
		static String status = "To Do";
		static String StatusName = "Review";
		static String StatuslistName = "";
        static String FilePath;
        
        public  void CreateDailogubox() {
			  
			JFrame frame = new JFrame("JSON File Reader");
			 FilePath = JOptionPane.showInputDialog(frame, "Enter the File Path");
			System.out.println(FilePath);
		}
		
		public static void main(String args[]) throws Exception {
			System.out.println("Start Test...");
			CloudBLM_Smoke_Project test_1 = new CloudBLM_Smoke_Project();
			test_1.CreateDailogubox();
			System.out.println(FilePath);
			String pathFile =FilePath;
			System.out.println(pathFile);

			src = new File(FilePath+"\\CloudBLM_DEV_SuitConfig.xls");
		//	src = new File(FilePath);
			fis = new FileInputStream(src);
			wb = new HSSFWorkbook(fis);
			sh1 = wb.getSheetAt(0);
			sh2 = wb.getSheetAt(1);
			Thread.sleep(2000);
		//	report = new ExtentReports("W:\\Fazil\\CloubBLM Suite\\CloudBLM Smoke Test Report.html");
			overallstartTime = System.nanoTime();
			System.out.println(sh2.getRow(2).getCell(2).getStringCellValue());
			Build = sh2.getRow(2).getCell(2).getStringCellValue();
			report = new ExtentReports("W:\\Fazil\\CloubBLM Suite\\CloudBLM Smoke Test Report "+Build+".html");
			test = report.startTest("Cloud BLM");		
			test_1.ChromeBrowser();
			test_1.AdminLogin();
			test_1.NewUser();
			test_1.UserTeam();
			test_1.Roles();
			test_1.organization();
			test_1.Status();
			test_1.Workflow();
			test_1.AdminLogout();
			test_1.AppVersion_LoginPage();
			test_1.LoginPage();
			test_1.CreateProject();
			test_1.ManageProject();
			test_1.ProjectCardView(); 
			test_1.ProjectGridView();
			test_1.CreateSubProject();
			test_1.SubProjectCardVeiw();
			test_1.SubProjectGridView();
			test_1.ProjectOverview();
			test_1.ModelViewer();
			test_1.Grouping();
			test_1.Createissues_Standalone();
			test_1.Createissues_PropertyBased();
			test_1.Createissues_ElementBased();
			test_1.Manageissues();
			test_1.ManageRule();
			test_1.ContentManager();
			test_1.ManageRFI();
			test_1.Logout();
			test_1.End();
	        CloudBLM_Smoke_Project.SendMail();             
		}				 
		
		public void ChromeBrowser() throws InterruptedException, SecurityException, IOException {

			// Launch the browser
			System.setProperty("webdriver.chrome.driver", "Input/chromedriver.exe");
			driver = new ChromeDriver();
			// Launched the Cloud BLM URL
			driver.manage().window().maximize();
			System.out.println(sh2.getRow(2).getCell(1).getStringCellValue());
			String URL = sh2.getRow(2).getCell(1).getStringCellValue();
			driver.get(URL);
			Thread.sleep(2000);
		}
	
		public void AdminLogin() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(2).getCell(2).getStringCellValue());
			String Input = sh1.getRow(2).getCell(2).getStringCellValue();
			String[] splits = Input.split(";|=");
			String Username = splits[1];
			String Password = splits[3];

			// Get the status row
			String TestStatus = "Yes";
			long startTime = 0;
			String Status = sh1.getRow(2).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				try {
					startTime = System.nanoTime();
					driver.findElement(By.xpath("//*[@id='login-email']")).sendKeys(Username);
					driver.findElement(By.id("login-password")).sendKeys(Password);
					driver.findElement(By.xpath("//button[text()='Sign In']")).click();
					Thread.sleep(3000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);

					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "Admin Login");
					System.out.println(time);
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\Admin Login.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "Admin Login" + "<br />" + "<b style='color:#FF0000';>Exception: </b>" + e
							+ "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}

			} else {
				System.out.println("Change the Status in Admin Login");
				test.log(LogStatus.SKIP, "Admin Login");
	
			}
		}

		public void NewUser() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(3).getCell(2).getStringCellValue());
			String Input = sh1.getRow(3).getCell(2).getStringCellValue();
			String[] splits = Input.split(";|=");
			String Name = splits[1];
			String UserName = splits[3];
			String EmailID = splits[5];
			// String Organization = splits[7];

			// Get the status row
			String TestStatus = "Yes";
			long startTime = 0;
			String Status = sh1.getRow(3).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				try {
					startTime = System.nanoTime();
					// click the Adminstration
					driver.findElement(By.id("Admin")).click();
					Thread.sleep(2000);
					// Click the organization button
					driver.findElement(By.id("Users")).click();
					Thread.sleep(3000);
					// click the create button to create the new user
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);
					// Enter the Name
					driver.findElement(By.xpath("//input[@placeholder='Enter a Name']")).sendKeys(Name);
					Thread.sleep(2000);
					// Enter the username
					driver.findElement(By.xpath("//input[@placeholder='Enter an Name']")).sendKeys(UserName);
					Thread.sleep(2000);
					// Enter the email Id
					driver.findElement(By.xpath("//input[@placeholder='Enter an Email address']")).sendKeys(EmailID);
					Thread.sleep(2000);
					// Enter the Organization
					// driver.findElement(By.xpath("//select[@formcontrolname='OrganizationID']")).sendKeys(Organization);
					// Thread.sleep(2000);
					// Click the cancel button
					// driver.findElement(By.xpath("//button[@blmtooltip='Cancel']")).click();
					Thread.sleep(2000);
					// Clicks the create button
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);
					// click the Adminstration
					driver.findElement(By.id("Admin")).click();
					Thread.sleep(2000);
					// clicks the Dashboard
					driver.findElement(By.id("Dashboard")).click();
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "New User");
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\New User.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "New User" + "<br />" + "<b style='color:#FF0000';>Exception: </b>" + e
							+ "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
				}

			} else {
				System.out.println("Change the Status in New User");
				test.log(LogStatus.SKIP, "New User");				
			}
		}

		public void UserTeam() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(4).getCell(2).getStringCellValue());
			String Input = sh1.getRow(4).getCell(2).getStringCellValue();
			String[] splits = Input.split(";|=");
			String MapUser = splits[1];
			String RoleName = splits[3];
			String Input_1 = sh1.getRow(3).getCell(2).getStringCellValue();
			String[] splits_1 = Input_1.split(";|=");
			// String Name = splits_1[1];
			String UserName = splits_1[3];
			// Get the status row
			String TestStatus = "Yes";
			String Status = sh1.getRow(4).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				long startTime = 0;
				try {
					startTime = System.nanoTime();
					// click the Adminstration
					driver.findElement(By.id("Admin")).click();
					Thread.sleep(5000);
					// click the teams tab
					driver.findElement(By.xpath("//span[text()='Teams']")).click();
					Thread.sleep(8000); // selct the respective role and add the user
					driver.findElement(By.xpath("//td[text()='" + RoleName + "']/following::td[3]")).click();
					Thread.sleep(5000);
					// click the add user icon and search the user
					driver.findElement(By.xpath("(//input[@placeholder='Search'])[2]")).sendKeys(MapUser);
					// select the respective user
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//input[@type='checkbox'])[5]")).click();
					Thread.sleep(2000);
					// Select the invite options
					driver.findElement(By.xpath("(//input[@type='checkbox'])[6]")).click();
					Thread.sleep(2000);
					// clicks the Add user button to add the user
					driver.findElement(By.xpath("//button[@class='blm-btn-create']")).click();
					Thread.sleep(2000);
					// clicks the cancel button
					// driver.findElement(By.xpath("//button[@blmtooltip='Cancel']")).click();
					Thread.sleep(5000);
					driver.findElement(By.id("Users")).click();
					Thread.sleep(5000);
					// filter the user
					WebElement myelement = driver
							.findElement(By.xpath("(//div[@class='e-filtermenudiv e-icons e-icon-filter'])[1]"));
					JavascriptExecutor jse2 = (JavascriptExecutor) driver;
					jse2.executeScript("arguments[0].click();", myelement);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@placeholder='Enter the value']")).sendKeys(UserName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//span[@role='listbox'])[2]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//span[@role='listbox'])[2]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[text()='Filter']")).click();
					Thread.sleep(2000);

					// Clicks the team tab to remove the user in Team tab
					driver.findElement(By.id("Teams")).click();
					Thread.sleep(5000);
					driver.findElement(By.xpath("(//div[@class='e-icons e-dtdiagonalright e-icon-grightarrow'])[1]"))
							.click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//td[text()='  " + UserName + "  ']//following::span[1]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[text()='YES']")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("Users")).click();
					Thread.sleep(2000);
					WebElement myelement1 = driver
							.findElement(By.xpath("(//div[@class='e-filtermenudiv e-icons e-icon-filter'])[1]"));
					JavascriptExecutor jse21 = (JavascriptExecutor) driver;
					jse21.executeScript("arguments[0].click();", myelement1);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@placeholder='Enter the value']")).sendKeys(UserName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//span[@role='listbox'])[2]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//span[@role='listbox'])[2]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[text()='Filter']")).click();
					Thread.sleep(2000);	
					  // clicks the edit buttom
					 /* driver.findElement(By.xpath("(//span[@placement='bottom'])[1]")).click();
					  Thread.sleep(2000); // Select the organization
					  driver.findElement(By.xpath("//select[@formcontrolname='OrganizationID']")).
					  sendKeys(Organizationname); Thread.sleep(2000); // clicks the update button
					  driver.findElement(By.xpath("//button[@blmtooltip='Update']")).click();
					  Thread.sleep(3000); WebElement myelement11 = driver .findElement(By.
					  xpath("(//div[@class='e-filtermenudiv e-icons e-icon-filter'])[1]"));
					  JavascriptExecutor jse211 = (JavascriptExecutor) driver;
					  jse211.executeScript("arguments[0].click();", myelement11);
					  Thread.sleep(2000);
					  driver.findElement(By.xpath("//input[@placeholder='Enter the value']")).
					  sendKeys(UserName); Thread.sleep(3000);
					  driver.findElement(By.xpath("//button[text()='Filter']")).click();
					  Thread.sleep(2000);*/
					 
					// click the Adminstration
					driver.findElement(By.id("Admin")).click();
					Thread.sleep(2000);

					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);

					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "User Team");
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\User Team.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "User Team" + "<br />" + "<b style='color:#FF0000';>Exception: </b>" + e
							+ "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in User Team");
				test.log(LogStatus.SKIP, "User Team");
				
			}

		}

		public void Roles() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(5).getCell(2).getStringCellValue());
			String Input = sh1.getRow(5).getCell(2).getStringCellValue();
			String[] splits = Input.split(";|=");
			String Rolename = splits[1];
			String Description = splits[3];

			// Get the status row
			String TestStatus = "Yes";

			String Status = sh1.getRow(5).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				long startTime = 0;
				try {
					startTime = System.nanoTime();
					// clicks the Dashboard
					driver.findElement(By.id("Dashboard")).click();
					Thread.sleep(2000);
					// click the Adminstration
					driver.findElement(By.id("Admin")).click();
					Thread.sleep(3000);
					// click the teams tab
					driver.findElement(By.xpath("//span[text()='Global Roles']")).click();
					Thread.sleep(3000);
					// click the crearte button
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(3000);
					// Enter the role name
					driver.findElement(By.xpath("//input[@name='roleName']")).sendKeys(Rolename);
					// enter the description
					driver.findElement(By.xpath("//textarea[@name='description']")).sendKeys(Description);
					Thread.sleep(3000);
					driver.findElement(By.id("role_0")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[@blmtooltip='Create']")).click();
					Thread.sleep(2000);
					WebDriverWait wait = new WebDriverWait (driver, 20);
					WebElement text = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//span[text()='Description'])[1]")));
					/*
					 * WebElement myelement1 = driver .findElement(By.
					 * xpath("(//div[@class='e-filtermenudiv e-icons e-icon-filter'])[1]"));
					 * JavascriptExecutor jse21 = (JavascriptExecutor) driver;
					 * jse21.executeScript("arguments[0].click();", myelement1); Thread.sleep(2000);
					 */
			//		WebElement myelement12 = driver.findElement(By.xpath("(//span[text()='Description'])[1]"));
					Actions act = new Actions(driver);
					act.moveToElement(text).build().perform();
					Thread.sleep(2000);
					System.out.println(1);
					driver.findElement(By.xpath("(//div[@class='e-filtermenudiv e-icons e-icon-filter'])[3]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@placeholder='Enter the value']")).sendKeys(Description);
					Thread.sleep(2000);
					WebElement clickstartwith = driver.findElement(By.xpath("(//span[@role='listbox'])[2]"));
					clickstartwith.click();
					Thread.sleep(1000);
					clickstartwith.click();
					Thread.sleep(1000);
					driver.findElement(By.xpath("//button[text()='Filter']")).click();
					// Check the Role Name
					String GetRoleName = driver.findElement(By.xpath("(//td[@role='gridcell'])[1]")).getText();
					if (GetRoleName.equals(Rolename)) {
						test.log(LogStatus.PASS, "Roles Name");
					} else {
						test.log(LogStatus.FAIL, "Roles Name");
					}
					// Assert.assertEquals(GetRoleName, Rolename);
					// Check the Role Description
					String GetDescription = driver.findElement(By.xpath("(//td[@role='gridcell'])[3]")).getText();
					if (GetDescription.equals(Description)) {
						test.log(LogStatus.PASS, "Roles Description");
					} else {
						test.log(LogStatus.FAIL, "Roles Description");
					}
					// Assert.assertEquals(GetDescription, Description);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "Roles");
					System.out.println(time);
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\RolesandOrganizations.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "Roles" + "<br />" + "<b style='color:#FF0000';>Exception: </b>" + e + "<br />"
							+ test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in Roles");
				test.log(LogStatus.SKIP, "Roles");
				
			}

		}

		public void organization() throws ResourceNotFoundException, ParseErrorException, MethodInvocationException,
				IOException, AWTException, InterruptedException {
			System.out.println(sh1.getRow(6).getCell(2).getStringCellValue());
			String Input = sh1.getRow(6).getCell(2).getStringCellValue();
			String[] splits = Input.split(";|=");
			String Organizationname = splits[1];
			String Country = splits[3];
			String OrganizationDescription = splits[5];
			String WorkflowRoleName = splits[7];
			String WorkflowRoleDescription = splits[9];
			
			System.out.println(sh1.getRow(7).getCell(2).getStringCellValue());
			String Input11 = sh1.getRow(7).getCell(2).getStringCellValue();
			String[] splits11 = Input11.split(";|=");
			String orgName = splits11[1];
			String orgUserName = splits11[3];
			String orgEmailID = splits11[5];

			String Input1 = sh1.getRow(3).getCell(2).getStringCellValue();
			String[] splits1 = Input1.split(";|=");
			String UserName = splits1[3];
		

			// Get the status row
			String TestStatus = "Yes";

			String Status = sh1.getRow(6).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				long startTime = 0;
				try {
					startTime = System.nanoTime();

					// Click on the Organizations
					driver.findElement(By.id("Organizations")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@formcontrolname='OrganizationName']")).sendKeys(Organizationname);
					Thread.sleep(2000);
					// Enter the website url
					driver.findElement(By.xpath("//input[@formcontrolname='OrganizationURL']"))
							.sendKeys("www.cloudBLM.com");
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@formcontrolname='CountryName']")).sendKeys(Country);
					// Enter the industry value
					driver.findElement(By.xpath("//input[@placeholder='Enter a Industry']")).sendKeys("Construction");
					Thread.sleep(2000);
					driver.findElement(By.xpath("//textarea[@placeholder='Enter your Description']"))
							.sendKeys(OrganizationDescription);
					Thread.sleep(2000);
					// Clicks the create button
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(8000);
					driver.findElement(By.id("Users")).click();
					Thread.sleep(2000);
					WebElement myelement1 = driver
							.findElement(By.xpath("(//div[@class='e-filtermenudiv e-icons e-icon-filter'])[1]"));
					JavascriptExecutor jse21 = (JavascriptExecutor) driver;
					jse21.executeScript("arguments[0].click();", myelement1);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@placeholder='Enter the value']")).sendKeys(UserName);
					Thread.sleep(3000);
					driver.findElement(By.xpath("//button[text()='Filter']")).click();
					Thread.sleep(8000);
					// clicks the edit button
					driver.findElement(By.xpath("(//span[@placement='bottom'])[1]")).click();
					Thread.sleep(2000);
					// Select the organization
					driver.findElement(By.xpath("//select[@formcontrolname='OrganizationID']")).sendKeys(Organizationname);
					Thread.sleep(2000);
					// clicks the update button
					driver.findElement(By.xpath("//button[@blmtooltip='Update']")).click();
					Thread.sleep(3000);
					String text = driver.findElement(By.xpath("//div[@class='toast-message ng-star-inserted']")).getText();
					System.out.println(text); 				
					if(text.equals("SUCCESS")) {
						System.out.println(text);
						test.log(LogStatus.PASS, "Organization Mapping");
					}else {
						test.log(LogStatus.FAIL, "Organization Mapping");
					}
					// Clicks the cancel button
					// driver.findElement(By.xpath("//button[@blmtooltip='Cancel']")).click();
					Thread.sleep(5000);
					driver.findElement(By.id("Organizations")).click();
					Thread.sleep(3000);
					// Filter the respective Organizations
					WebElement myelement121 = driver
							.findElement(By.xpath("(//div[@class='e-filtermenudiv e-icons e-icon-filter'])[1]"));
					JavascriptExecutor jse2111 = (JavascriptExecutor) driver;
					jse2111.executeScript("arguments[0].click();", myelement121);
					Thread.sleep(2000);

					driver.findElement(By.xpath("//input[@placeholder='Enter the value']"))
							.sendKeys(OrganizationDescription);
					Thread.sleep(2000);
					WebElement clickel23 = driver.findElement(By.xpath("//button[text()='Filter']"));
					clickel23.click();
					Thread.sleep(2000);
					// Check the Org Name
					String GetOrganizationName = driver.findElement(By.xpath("(//td[@role='gridcell'])[1]")).getText();
					if (GetOrganizationName.equals(Organizationname)) {
						test.log(LogStatus.PASS, "Organization Name");
					} else {
						test.log(LogStatus.FAIL, "Organization Name");
					}
					// Assert.assertEquals(GetOrganizationName, GetOrganizationName);
					// Check the org Description
					String GetOrganizationDescription = driver.findElement(By.xpath("(//td[@role='gridcell'])[3]"))
							.getText();
					// Assert.assertEquals(GetOrganizationDescription, OrganizationDescription);
					if (GetOrganizationDescription.equals(OrganizationDescription)) {
						test.log(LogStatus.PASS, "Organization Description");
					} else {
						test.log(LogStatus.FAIL, "Organization Description");
					}
					Thread.sleep(4000);
					// clicks the Setting icon
					driver.findElement(By.xpath("(//span[@placement='bottom'])[2]")).click();
					Thread.sleep(4000);
					// click the create button to create the user
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);
					// Enter the Name
					driver.findElement(By.xpath("//input[@placeholder='Enter a Name']")).sendKeys(orgName);
					Thread.sleep(2000);
					// Enter the username
					driver.findElement(By.xpath("//input[@placeholder='Enter an Name']")).sendKeys(orgUserName);
					Thread.sleep(2000);
					// Enter the email Id
					driver.findElement(By.xpath("//input[@placeholder='Enter an Email address']")).sendKeys(orgEmailID);
					Thread.sleep(2000);
					// Enter the Organization
					// driver.findElement(By.xpath("//select[@formcontrolname='OrganizationID']")).sendKeys(Organization);
					// Thread.sleep(2000);
					// Click the cancel button
					// driver.findElement(By.xpath("//button[@blmtooltip='Cancel']")).click();
					Thread.sleep(2000);
					// Clicks the create button
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);
					// clicks the teams button
					driver.findElement(By.id("OrgTeams")).click();
					Thread.sleep(2000);
					// Adding the organization for respective users
					driver.findElement(By.xpath("(//span[@placement='bottom'])[1]")).click();
					Thread.sleep(2000);
					// Serach the users
					driver.findElement(By.xpath("(//input[@placeholder='Search'])[2]")).sendKeys(orgEmailID);
					Thread.sleep(2000);
					// Select the user
					driver.findElement(By.xpath("(//input[@placeholder='Search'])[2]//following::input[1]")).click();
					Thread.sleep(2000);
					// Select the Email invite checkbox
					driver.findElement(By.xpath("(//input[@placeholder='Search'])[2]//following::input[2]")).click();
					Thread.sleep(2000);

					// clicks the add user button
					driver.findElement(By.xpath("//button[@class='blm-btn-create']")).click();
					Thread.sleep(2000);
					// clicks the side bar in Organization Administrator
					driver.findElement(By.xpath("(//div[@class='e-icons e-dtdiagonalright e-icon-grightarrow'])[1]"))
							.click();
					Thread.sleep(4000);
					// Clicks the role Menu to create the role
					driver.findElement(By.id("OrgRoles")).click();
					Thread.sleep(2000);
					// click the crearte button
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(3000);
					// Enter the role name
					driver.findElement(By.xpath("//input[@name='roleName']")).sendKeys(WorkflowRoleName);
					// enter the description
					driver.findElement(By.xpath("//textarea[@name='description']")).sendKeys(WorkflowRoleDescription);
					Thread.sleep(3000);
					driver.findElement(By.id("role_5")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[@blmtooltip='Create']")).click();
					Thread.sleep(8000);
					WebElement myelement12 = driver.findElement(By.xpath("(//span[text()='Description'])[1]"));
					Actions act = new Actions(driver);
					act.moveToElement(myelement12).build().perform();
					Thread.sleep(2000);
					System.out.println(1);
					driver.findElement(By.xpath("(//div[@class='e-filtermenudiv e-icons e-icon-filter'])[3]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@placeholder='Enter the value']")).sendKeys(WorkflowRoleDescription);
					Thread.sleep(2000);
					WebElement clickstartwith = driver.findElement(By.xpath("(//span[@role='listbox'])[2]"));
					clickstartwith.click();
					Thread.sleep(1000);
					clickstartwith.click();
					Thread.sleep(1000);
					driver.findElement(By.xpath("//button[text()='Filter']")).click();
					Thread.sleep(2000);

					test.log(LogStatus.PASS, "Organizations");
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\Organizations.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "Organizations" + "<br />"
							+ "<b style='color:#FF0000';>Exception: </b>" + e + "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in Roles");
				test.log(LogStatus.SKIP, "Organizations");
				
			}

		}

		public void Status() throws Exception {
			// Get the status row
			String TestStatus = "Yes";

			String Status = sh1.getRow(8).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				long startTime = 0;
				try {
					startTime = System.nanoTime();
					driver.findElement(By.id("OrgStatus")).click();
					Thread.sleep(2000);

					List<WebElement> StatusCount = driver
							.findElements(By.xpath("//td[@aria-label=' is template cell column header Status']"));
					Thread.sleep(2000);
					for (int i = 0; i < StatusCount.size(); i++) {
						System.out.println(StatusCount.get(i).getText());
						StatuslistName = StatuslistName + StatusCount.get(i).getText() + ";";
					}
					System.out.println(StatuslistName);
					System.out.println(1);
					String[] Splitstatus = StatuslistName.split(";");
					String Status1 = Splitstatus[0];
					System.out.println(Status1);
					String Status2 = Splitstatus[1];
					System.out.println(Status2);
					String Status3 = Splitstatus[2];
					System.out.println(Status3);
					String[] ActualStatusName = { "To Do (Default)", "In Progress (Default)", "Done (Default)" };
					String[] ExpectedStatusName = { Status1, Status2, Status3 };
					System.out.println("PAss");
					for (int i = 0; i <= 2; i++) {
						if (ActualStatusName[i].equals(ExpectedStatusName[i])) {
							System.out.println("Both Status Name is Matched");
						} else {
							System.out.println("Both Status Name is not Matched");
						}
					}
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);

					// Enter the Status Name
					driver.findElement(By.xpath("//input[@placeholder='Status Name']")).sendKeys(StatusName);
					Thread.sleep(2000);
					// Select the Category
					driver.findElement(By.xpath("//select[@formcontrolname='category']")).sendKeys(status);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//textarea[@placeholder='Type your Description Here']"))
							.sendKeys(StatusName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[text()='ADD ']")).click();
					Thread.sleep(2000);
					WebElement Motofilter1 = driver.findElement(By.xpath("(//span[text()='Status'])[2]"));
					Actions ms1 = new Actions(driver);
					ms1.moveToElement(Motofilter1).build().perform();
					Thread.sleep(3000);
					driver.findElement(By.xpath("(//div[@class='e-filtermenudiv e-icons e-icon-filter'])[1]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@placeholder='Enter the value']")).sendKeys(StatusName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[text()='Filter']")).click();
					Thread.sleep(2000);
					// validatate the StatusName
					WebElement GetStatusName = driver.findElement(By.xpath("(//span[@placement='bottom'])[2]"));
					if (GetStatusName.getText().equals(StatusName)) {
						System.out.println("Both Status Names are equal");
					} else {
						System.out.println("Both Status Names are not equal");
					}
					Thread.sleep(2000);
					// Validate the description
					WebElement GetDescriptionName = driver.findElement(By.xpath("(//span[@placement='bottom'])[3]"));
					if (GetDescriptionName.getText().equals(StatusName)) {
						System.out.println("Both Status Descriptions are equal");
					} else {
						System.out.println("Both Status Descriptions are not equal");
					}
					Thread.sleep(2000);
					// validate the category
					WebElement GetCateoryName = driver.findElement(By.xpath("(//span[@placement='bottom'])[4]"));
					if (GetCateoryName.getText().equals(status)) {
						System.out.println("Both Status Category are equal");
					} else {
						System.out.println("Both Status Category are not equal");
					}
					// Clicks the Edit icon
					driver.findElement(By.xpath("(//a[@placement='bottom'])[1]")).click();
					Thread.sleep(2000);
					// Clicks the Default checkbox
					driver.findElement(By.xpath("//input[@formcontrolname='isDefault']")).click();
					Thread.sleep(2000);
					// clicks the update buton
					driver.findElement(By.xpath("//button[text()='UPDATE ']")).click();
					Thread.sleep(2000);
					// Validate the Status Name
					WebElement GetStatusName1 = driver.findElement(By.xpath("(//td[@role='gridcell'])[2]"));
					System.out.println(GetStatusName1.getText());
					if (GetStatusName1.getText().equals(StatusName + " (Default)")) {
						System.out.println("Both Status Names are equal");
					} else {
						System.out.println("Both Status Names are not equal");
					}
					// Clear the filter
					WebElement Motofilter11 = driver.findElement(By.xpath("(//span[text()='Status'])[2]"));
					Actions ms11 = new Actions(driver);
					ms11.moveToElement(Motofilter11).build().perform();
					Thread.sleep(3000);
					driver.findElement(By.xpath("(//div[@class='e-filtermenudiv e-icons e-icon-filter e-filtered'])"))
							.click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[text()='Clear']")).click();
					Thread.sleep(2000);

					// Clicks the create button to validate the duplicate message
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);

					// Enter the Status Name
					driver.findElement(By.xpath("//input[@placeholder='Status Name']")).sendKeys(StatusName);
					Thread.sleep(2000);

					// Select the Category
					driver.findElement(By.xpath("//select[@formcontrolname='category']")).sendKeys(status);
					Thread.sleep(2000);

					driver.findElement(By.xpath("//textarea[@placeholder='Type your Description Here']"))
							.sendKeys(StatusName);
					Thread.sleep(2000);

					driver.findElement(By.xpath("//button[text()='ADD ']")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[@blmtooltip='Cancel']")).click();
					Thread.sleep(2000);

					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);

					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "Status");
					System.out.println(time);
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\Status.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "Status" + "<br />" + "<b style='color:#FF0000';>Exception: </b>" + e
							+ "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in Statusflow");
				test.log(LogStatus.SKIP, "Status");
			}
		}

		public void Workflow() throws Exception {
			// Get the status row
			String TestStatus = "Yes";
			String Status = sh1.getRow(9).getCell(4).getStringCellValue();
			
			System.out.println(sh1.getRow(6).getCell(2).getStringCellValue());
			String Input = sh1.getRow(6).getCell(2).getStringCellValue();
			String[] splits = Input.split(";|=");
			String WorkflowRoleName = splits[7];
			
			String Input1 = sh1.getRow(9).getCell(2).getStringCellValue();
			String[] splits1 = Input1.split(";|=");
			String WorkflowName = splits1[1];
			String WorkflowDescription = splits1[3];
			
			if (Status.equals(TestStatus)) {
				long startTime = 0;
				try {
					String[] Status1 = { "In Progress", "Done" };
					String[] Role = { WorkflowRoleName, WorkflowRoleName };

					startTime = System.nanoTime();
					driver.findElement(By.id("OrgWorkflow")).click();
					Thread.sleep(3000);
					// Clicks the add icon
					driver.findElement(By.xpath("(//div[@placement='bottom'])[2]//following::span[2]")).click();
					Thread.sleep(2000);

					driver.findElement(By.xpath("//input[@placeholder='Workflow Name']")).sendKeys(WorkflowName);

					// Enter the Description
					driver.findElement(By.xpath("//textarea[@placeholder='Type your Description Here']")).sendKeys(WorkflowDescription);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[@class='blm-btn-create']")).click();
					Thread.sleep(8000);
					//
					driver.findElement(By.id("diagramcontent")).click();
					Thread.sleep(3000);
					driver.findElement(By.xpath("//*[text()='" + StatusName + "']")).click();
					Thread.sleep(4000);
					WebElement Editelement = driver.findElement(By.xpath("(//*[@id='orgEditHandle_userhandle'])"));
					Actions act = new Actions(driver);
					act.moveToElement(Editelement).click().build().perform();
					Thread.sleep(4000);
					// Clicks the respective status
					// driver.findElement(By.xpath("//input[@aria-placeholder='Select
					// Status']")).click();
					driver.findElement(By.id("multiselect-checkbox")).click();
					Thread.sleep(4000);

					WebElement SearchRole = driver.findElement(By.cssSelector("input.e-input-filter.e-input"));
					SearchRole.click();
					SearchRole.sendKeys(WorkflowRoleName);
					Thread.sleep(3000);
					driver.findElement(By.cssSelector("div.e-content.e-dropdownbase")).click();
					Thread.sleep(2000);
					//// ul[@id='multiselect-checkbox_options']/li
					// Clicks the apply button
					driver.findElement(By.xpath("//button[@class='blm-btn-create']")).click();
					Thread.sleep(2000);
					for (int i = 0; i < 2; i++) {

						WebElement Addelement = driver.findElement(By.xpath("(//*[@id='orgAddHandle_userhandle'])"));
						Actions act1 = new Actions(driver);
						act1.moveToElement(Addelement).click().build().perform();
						Thread.sleep(4000);
						driver.findElement(By.xpath("//*[text()='New Status']")).click();
						Thread.sleep(4000);
						WebElement Editelement1 = driver.findElement(By.xpath("(//*[@id='orgEditHandle_userhandle'])"));
						Actions act11 = new Actions(driver);
						act11.moveToElement(Editelement1).click().build().perform();
						Thread.sleep(4000);
						// Select the Status
						driver.findElement(By.xpath("//span[@class='e-input-group-icon e-ddl-icon e-search-icon']"))
								.click();
						Thread.sleep(2000);
						WebElement SearchStatus = driver.findElement(By.cssSelector("input.e-input-filter.e-input"));
						SearchStatus.click();
						SearchStatus.sendKeys(Status1[i]);
						Thread.sleep(3000);
						driver.findElement(By.cssSelector("div.e-content.e-dropdownbase")).click();
						Thread.sleep(2000);
						// Select the Role
						driver.findElement(By.id("multiselect-checkbox")).click();
						Thread.sleep(4000);

						WebElement SearchRole1 = driver.findElement(By.cssSelector("input.e-input-filter.e-input"));
						SearchRole1.click();
						SearchRole1.sendKeys(Role[i]);
						Thread.sleep(3000);
						driver.findElement(By.cssSelector("div.e-content.e-dropdownbase")).click();
						Thread.sleep(2000);
						// Clicks the apply button
						driver.findElement(By.xpath("//button[@class='blm-btn-create']")).click();
					}
					Thread.sleep(2000);
					// Clicks the Save button
					driver.findElement(By.xpath("//span[text()='Save']")).click();
					Thread.sleep(2000);

					Thread.sleep(2000);
					driver.findElement(By.xpath("//div[text()='Issue Workflow Scheme ']")).click();
					Thread.sleep(2000);
					// Edit the Workflow
					driver.findElement(By.xpath("//span[text()='"+WorkflowName+"']//following::span[1]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@name='isDefault']")).click();
					Thread.sleep(2000);
					// Clicks the Save button
					driver.findElement(By.xpath("//span[text()='Save']")).click();

					Thread.sleep(2000);
					// Clicks the BAck to scheme option
					driver.findElement(By.xpath("//span[text()='Back to schemes']")).click();
					Thread.sleep(2000);
					// Duplicate workflow
					driver.findElement(By.xpath("//div[text()='Issue Workflow Scheme ']")).click();
					Thread.sleep(2000);
					// Edit the Workflow
					driver.findElement(By.xpath("//span[text()='"+WorkflowName+"']//following::span[2]")).click();
					Thread.sleep(2000);
					// clicks the create button to validate the toast message
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@formcontrolname='workflowName']")).clear();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@formcontrolname='workflowName']")).sendKeys(WorkflowName+1);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@name='isDefalut']")).click();
					Thread.sleep(2000);
					// clicks the create button
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);
					// Delete the Duplicate Workscheme

					driver.findElement(By.xpath("//span[text()='"+WorkflowName+1+"']//following::span[2]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[text()='Yes']")).click();
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "WorkFlow");
					System.out.println(time);
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\WorkFlow.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "WorkFlow" + "<br />" + "<b style='color:#FF0000';>Exception: </b>" + e
							+ "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in WorkFlow");
				test.log(LogStatus.SKIP, "WorkFlow");
			}
		}

		public void AdminLogout() throws InterruptedException, SecurityException, IOException, AWTException {

			// Get the status row
			String TestStatus = "Yes";

			String Status = sh1.getRow(10).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				long startTime = 0;
				try {
					startTime = System.nanoTime();
					driver.findElement(By.xpath("//a[@data-toggle='dropdown']")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//a[text()='Logout']")).click();
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);

					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "Admin Logout");
					System.out.println(time);
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\Admin Logout.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "Admin Logout" + "<br />" + "<b style='color:#FF0000';>Exception: </b>" + e
							+ "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in Admin Logout");
				test.log(LogStatus.SKIP, "Admin Logout");
			}
		}

		public void AppVersion_LoginPage() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(11).getCell(2).getStringCellValue());
			String AppVersion = sh1.getRow(11).getCell(2).getStringCellValue();
			// Get the status row
			String TestStatus = "Yes";
			long startTime = 0;
			String Status = sh1.getRow(11).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				try {
					startTime = System.nanoTime();
					Thread.sleep(1000);
					String GetAppVersion = driver.findElement(By.xpath("(//span[@class='px-2 appVersionStyle'])[2]"))
							.getText();
					System.out.println("AppVersion_" + GetAppVersion);
				//	Assert.assertEquals(AppVersion, GetAppVersion);
					if (AppVersion.equals(GetAppVersion)) {
						System.out.println("pass");
					} else {
						System.out.println("fail");
						long endTime = System.nanoTime();
						long totalTime = endTime - startTime;
						System.out.println(totalTime);
						long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
						long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
						System.out.println(minutes + "min" + convert + "sec");
						String time = minutes + " min, " + convert + " sec";
						context.put("AppVersionLogin_ExecutedTest", "Yes");
						context.put("AppVersionLogin_Result", "Fail");
						context.put("AppVersionLogin_time", time);
						StringWriter writer = new StringWriter();
						templates.merge(context, writer);
						String htmlReportes = writer.toString();
						generateHtmlas(htmlReportes, "W:\\Fazil\\CloubBLM Suite\\CloudBLM Smoke Email Template.html");
						String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\AppVersion Login.png";
						Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
						Robot robot = new Robot();
						BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
						ImageIO.write(screenFullImage, "png", new File(fileName));
						Thread.sleep(2000);
						test.log(LogStatus.FAIL, "AppVersion Login" + "<br />"
								+ "<b style='color:#FF0000';>Exception: </b>" + "<br />" + test.addScreenCapture(fileName));
					}
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);

					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "AppVersion Login");
					System.out.println(time);
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\AppVersion Login.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "AppVersion Login" + "<br />" + "<b style='color:#FF0000';>Exception: </b>" + e
							+ "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in AppVersion Login");
				test.log(LogStatus.SKIP, "AppVersion Login");
				
			}

		}

		public void LoginPage() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(12).getCell(2).getStringCellValue());
			String Input = sh1.getRow(12).getCell(2).getStringCellValue();
			String[] splits = Input.split(";|=");
			String EmailID = splits[5];
			String Password = splits[7];

			// Get the status row
			String TestStatus = "Yes";
			long startTime = 0;
			String Status = sh1.getRow(12).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				try {
					startTime = System.nanoTime();
					driver.findElement(By.xpath("//*[@id='login-email']")).sendKeys(EmailID);
					Thread.sleep(2000);
					driver.findElement(By.id("login-password")).sendKeys(Password);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[text()='Sign In']")).click();
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);

					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "LoginPage");
					System.out.println(time);
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\LoginPage.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "LoginPage" + "<br />" + "<b style='color:#FF0000';>Exception: </b>" + e
							+ "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in Login Page");
				test.log(LogStatus.SKIP, "LoginPage");
			
			}
		}

		public void CreateProject() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(13).getCell(2).getStringCellValue());
			String Input = sh1.getRow(13).getCell(2).getStringCellValue();
			String[] splits = Input.split(";|=");
			String ProjectName = splits[1];
			String ProjectType = splits[3];
			String ConstructionType = splits[5];
			String ContactType = splits[7];
			String StartDate = splits[9];
			String EndDate = splits[11];
			String Address = splits[13];
			String Description = splits[15];
			// Get the status row
			String TestStatus = "Yes";
			String Status = sh1.getRow(13).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				long startTime = 0;
				try {
					startTime = System.nanoTime();
					// clicks the project icon
					driver.findElement(By.xpath("//span[text()='Projects']")).click();
					Thread.sleep(2000);
					// clicks the manage projects
					driver.findElement(By.id("ManageProjects")).click();
					Thread.sleep(2000);
					// Create the project name
					driver.findElement(By.xpath("//button[@class='blm-btn-create']")).click();
					Thread.sleep(4000);
					driver.findElement(By.xpath("//*[@id='projectName']")).sendKeys(ProjectName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//*[@formcontrolname='ProjectTypeID']")).sendKeys(ProjectType);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//*[@formcontrolname='ConstructionTypeID']")).sendKeys(ConstructionType);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//*[@formcontrolname='ContractTypeID']")).sendKeys(ContactType);
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//*[@class ='e-input e-lib e-keyboard'])[1]")).sendKeys(StartDate);
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//*[@class ='e-input e-lib e-keyboard'])[2]")).sendKeys(EndDate);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//*[@formcontrolname ='ProjectAddress']")).sendKeys(Address);
					Thread.sleep(2000);
					driver.findElement(By.id("Description_rte-edit-view")).sendKeys(Description);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[@class='blm-btn-create']")).click();
					Thread.sleep(4000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "Create Project");
					System.out.println(time);
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\Create Project.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "Create Project" + "<br />" + "<b style='color:#FF0000';>Exception: </b>" + e
							+ "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in Create Project");
				test.log(LogStatus.SKIP, "Create Project");
			
			}
		}

		public void ManageProject() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(14).getCell(2).getStringCellValue());
			String Input = sh1.getRow(14).getCell(2).getStringCellValue();
			String[] splits = Input.split(";|=");
			String ProjectName = splits[1];
			System.out.println(ProjectName);
			// Get the status row
			String TestStatus = "Yes";

			String Status = sh1.getRow(14).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				long startTime = 0;
				try {
					startTime = System.nanoTime();

					Thread.sleep(2000);
					driver.findElement(By.id("facog")).click();
					Thread.sleep(5000);
					driver.findElement(By.xpath("//h3[text()='Filter']//following::input[1]")).sendKeys(ProjectName);
					Thread.sleep(3000);
					driver.findElement(By.xpath("(//i[@class='blm-icon-hamburger'])[3]")).click();
					Thread.sleep(3000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "Manage Project");
					System.out.println(time);
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\Manage Project.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "Manage Project" + "<br />" + "<b style='color:#FF0000';>Exception: </b>" + e
							+ "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in Manage Project");
				test.log(LogStatus.SKIP, "Manage Project");
			
			}

		}

		public void ProjectCardView() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(15).getCell(2).getStringCellValue());
			String Input = sh1.getRow(15).getCell(2).getStringCellValue();
			String[] splits = Input.split(";|=");
			String ProjectName = splits[1];
			String ProjectType = splits[3];
			String ConstructionType = splits[5];

			// Get the status row
			String TestStatus = "Yes";

			String Status = sh1.getRow(15).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				long startTime = 0;
				try {
					startTime = System.nanoTime();
					// check the project name is displayed or not in cardview
					String Getprojectname = driver.findElement(By.xpath("(//h5[@placement='bottom'])[1]")).getText();
					System.out.println("Project Name_" + Getprojectname);
					// Assert.assertEquals(ProjectName, Getprojectname);
					if (Getprojectname.equals(ProjectName)) {
						System.out.println("pass");
					} else {
						System.out.println("fail");
						long endTime = System.nanoTime();
						long totalTime = endTime - startTime;
						System.out.println(totalTime);
						long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
						long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
						System.out.println(minutes + "min" + convert + "sec");
						String time = minutes + " min, " + convert + " sec";
						context.put("Projectcardview_ExecutedTest", "Yes");
						context.put("Projectcardview_Result", "Fail");
						context.put("Projectcardview_Time", time);
						StringWriter writer = new StringWriter();
						templates.merge(context, writer);
						String htmlReportes = writer.toString();
						generateHtmlas(htmlReportes, "W:\\Fazil\\CloubBLM Suite\\CloudBLM Smoke Email Template.html");
						String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\Project CardView projectname.png";
						Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
						Robot robot = new Robot();
						BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
						ImageIO.write(screenFullImage, "png", new File(fileName));
						Thread.sleep(2000);
						test.log(LogStatus.FAIL, "Project CardView - Project Name" + "<br />"
								+ "<b style='color:#FF0000';>Exception: </b>" + "<br />" + test.addScreenCapture(fileName));
					}
					Thread.sleep(2000);

					// check the project type is displayed or not in card view
					String getprojecttype = driver.findElement(By.xpath("//p[text()='Project Type ']/following::td[2]"))
							.getText();
					System.out.println("Project Type_" + getprojecttype);
					Assert.assertEquals(getprojecttype, ProjectType);
					Thread.sleep(2000);

					// check the project subtype is displayed or not
					String projectsubtype = driver.findElement(By.xpath("//p[text()='Project Subtype ']/following::td[2]"))
							.getText();
					System.out.println("Project sub Type_" + projectsubtype);
					Assert.assertEquals(projectsubtype, projectsubtype);
					Thread.sleep(2000);

					// check the Construction type is displayed or not
					String Constructiontype = driver
							.findElement(By.xpath("//p[text()='Construction Type ']/following::td[2]")).getText();
					System.out.println("Construction Type" + Constructiontype);
					Assert.assertEquals(ConstructionType, Constructiontype);
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "Project CardView");
					System.out.println(time);
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\Project CardView.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "Project CardView" + "<br />" + "<b style='color:#FF0000';>Exception: </b>" + e
							+ "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in Project CardView");
				test.log(LogStatus.SKIP, "Project CardView");
			
			}
		}

		public void ProjectGridView() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(16).getCell(2).getStringCellValue());
			String Input = sh1.getRow(16).getCell(2).getStringCellValue();
			String[] splits = Input.split(";|=");
			String ProjectName = splits[1];
			String ProjectType = splits[3];
			String ConstructionType = splits[5];
			String StartDate = splits[9];
			String EndDate = splits[11];

			// Get the status row
			String TestStatus = "Yes";
			long startTime = 0;
			String Status = sh1.getRow(16).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				try {
					startTime = System.nanoTime();
					// click on the Grid icon
					driver.findElement(By.xpath("//i[@class='blm-icon-grid']")).click();
					Thread.sleep(2000);
					// mouse over to projectname
					WebElement elementactions1 = driver.findElement(By.xpath("//span[text()='Project Name']"));
					Actions act = new Actions(driver);
					act.moveToElement(elementactions1).perform();
					Thread.sleep(2000);
					// click the filter icon
					driver.findElement(By.xpath("(//div[@class='e-filtermenudiv e-icons e-icon-filter'])[1]")).click();
					Thread.sleep(2000);
					WebElement ele = driver.findElement(By.xpath("(//span[@role='listbox'])[2]"));
					ele.click();
					ele.sendKeys("Starts With");
					ele.click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@placeholder='Enter the value']")).sendKeys(ProjectName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[text()='Filter']")).click();
					// Check the project name
					String Getprojctname = driver.findElement(By.xpath("(//span[@placement='bottom'])[1]")).getText();
					System.out.println("Project Name_" + Getprojctname);
					Assert.assertEquals(Getprojctname, ProjectName);
					Thread.sleep(2000);
					// Get Project type
					String getprojecttype = driver.findElement(By.xpath("(//span[@placement='bottom'])[2]")).getText();
					System.out.println("Project Type_" + getprojecttype);
					Assert.assertEquals(getprojecttype, ProjectType);
					Thread.sleep(2000);
					// Get Project sub type
					String getsubprojecttype = driver.findElement(By.xpath("(//span[@placement='bottom'])[3]")).getText();
					System.out.println("Project Type_" + getsubprojecttype);
					Thread.sleep(2000);
					// Get Construction type
					String getconstructiontype = driver.findElement(By.xpath("(//span[@placement='bottom'])[4]")).getText();
					System.out.println("Construction Type_" + getconstructiontype);
					Thread.sleep(2000);
					Assert.assertEquals(getconstructiontype, ConstructionType);
					// Get the start date
					String getstartdate = driver.findElement(By.xpath("(//span[@placement='bottom'])[5]")).getText();
					System.out.println("Startdate_" + getstartdate);
					if (getstartdate.equals(StartDate)) {
						System.out.println("pass");
					} else {
						System.out.println("fail");
						long endTime = System.nanoTime();
						long totalTime = endTime - startTime;
						System.out.println(totalTime);
						long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
						long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
						System.out.println(minutes + "min" + convert + "sec");
						String time = minutes + " min, " + convert + " sec";
						System.out.println(time);
						String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\Project GridView startdate.png";
						Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
						Robot robot = new Robot();
						BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
						ImageIO.write(screenFullImage, "png", new File(fileName));
						Thread.sleep(4000);
						test.log(LogStatus.FAIL, "Project GridView Start Date" + "<br />"
								+ "<b style='color:#FF0000';>Exception: </b>" + "<br />" + test.addScreenCapture(fileName));
					}
					Thread.sleep(2000);
					// Get the End date
					String getenddate = driver.findElement(By.xpath("(//span[@placement='bottom'])[6]")).getText();
					System.out.println("Enddate_" + getenddate);
					if (getenddate.equals(EndDate)) {
						System.out.println("pass");
					} else {
						System.out.println("fail");
						long endTime = System.nanoTime();
						long totalTime = endTime - startTime;
						System.out.println(totalTime);
						long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
						long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
						System.out.println(minutes + "min" + convert + "sec");
						String time = minutes + " min, " + convert + " sec";
						System.out.println(time);
						String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\Project GridView enddate.png";
						Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
						Robot robot = new Robot();
						BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
						ImageIO.write(screenFullImage, "png", new File(fileName));
						Thread.sleep(4000);
						test.log(LogStatus.FAIL, "Project GridView End Date" + "<br />"
								+ "<b style='color:#FF0000';>Exception: </b>" + "<br />" + test.addScreenCapture(fileName));
					}
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "Project GridView");
					System.out.println(time);
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\Project GridView.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "Project GridView" + "<br />" + "<b style='color:#FF0000';>Exception: </b>" + e
							+ "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in Project GridView");
				test.log(LogStatus.SKIP, "Project GridView");
				
			}

		}

		public void CreateSubProject() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(13).getCell(2).getStringCellValue());
			String Input = sh1.getRow(13).getCell(2).getStringCellValue();
			// get the manage project value to filter the project
			String[] splits = Input.split(";|=");
			String SearchProjectName = splits[1];
			
			String getsubprojectinputvalues = sh1.getRow(17).getCell(2).getStringCellValue();
			String[] getsubprojectinput = getsubprojectinputvalues.split(";|=");
			String SubProjectName = getsubprojectinput[1];
		//	String ProjectType = getsubprojectinput[3];
			String Startdate = getsubprojectinput[5];
			String Enddate = getsubprojectinput[7];
			String Description = getsubprojectinput[9];
			// Get the status row
			String TestStatus = "Yes";
			long startTime = 0;
			String Status = sh1.getRow(17).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				try {
					startTime = System.nanoTime();
					driver.findElement(By.id("Dashboard")).click();
					Thread.sleep(3000);
					driver.findElement(By.xpath("//i[@class='blm-icon-project']")).click();
					Thread.sleep(3000);
					// clicks the manage projects
					driver.findElement(By.id("ManageProjects")).click();
					Thread.sleep(5000);
					driver.findElement(By.id("facog")).click();
					Thread.sleep(3000);
					driver.findElement(By.xpath("//h3[text()='Filter']//following::input[1]")).sendKeys(SearchProjectName);
					Thread.sleep(3000);
					driver.findElement(By.xpath("(//i[@class='blm-icon-hamburger'])[3]")).click();
					Thread.sleep(3000);

					// clicks the subproject icon
					driver.findElement(By.id("icon_3")).click();
					Thread.sleep(4000);
					driver.findElement(By.xpath("//button[@class='blm-btn-create']")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//*[@id='projectName']")).sendKeys(SubProjectName);
					Thread.sleep(2000);
					// clear the start date
					driver.findElement(By.xpath("//input[@placeholder='Select a start date']")).clear();
					Thread.sleep(2000);
					System.out.println(Startdate);
					driver.findElement(By.xpath("//input[@placeholder='Select a start date']")).sendKeys(Startdate);
					Thread.sleep(2000);
					System.out.println(Enddate);
					driver.findElement(By.xpath("//input[@placeholder='Select a end date']")).clear();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@placeholder='Select a end date']")).sendKeys(Enddate);
					Thread.sleep(2000);
					driver.findElement(By.id("Description_rte-edit-view")).sendKeys(Description);
					Thread.sleep(2000);
					// clicks the cancel button
					// driver.findElement(By.xpath("//button[@class='blm-btn-cancel']")).click();
					Thread.sleep(2000);
					// clicks the create button
					driver.findElement(By.xpath("//button[@class='blm-btn-create']")).click();
					Thread.sleep(2000);
					// driver.findElement(By.id("BackToProjects")).click();
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "Create SubProject");
					System.out.println(time);
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\Create SubProject.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "Create SubProject" + "<br />" + "<b style='color:#FF0000';>Exception: </b>"
							+ e + "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in Create SubProject");
				test.log(LogStatus.SKIP, "Create SubProject");
				
			}
		}

		public void SubProjectCardVeiw() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(18).getCell(2).getStringCellValue());
			String Input = sh1.getRow(18).getCell(2).getStringCellValue();
			String[] getsubprojectinput = Input.split(";|=");
			String SubProjectName = getsubprojectinput[1];
			String ProjectType = getsubprojectinput[3];
			String ConstructionType = getsubprojectinput[11];
			// Get the status row
			String TestStatus = "Yes";
			long startTime = 0;
			String Status = sh1.getRow(18).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				try {
					startTime = System.nanoTime();
					// filter the subproject
					driver.findElement(By.xpath("//div[@class='filter-enable pointer']")).click();
					Thread.sleep(5000);

					driver.findElement(By.xpath("//h3[text()='Filter']//following::input[1]")).sendKeys(SubProjectName);
					Thread.sleep(5000);
					driver.findElement(By.xpath("(//i[@class='blm-icon-hamburger'])[3]")).click();
					Thread.sleep(2000);
					// Get the subproject name
					String getsubprojectname = driver.findElement(By.xpath("//*[@placement='bottom']")).getText();
					System.out.println("Sub Project Name_" + getsubprojectname);
					// Assert.assertEquals(SubProjectName, getsubprojectname);

					// Get the suibproject type
					String getsubprojecttype = driver.findElement(By.xpath("//p[text()='Project Type ']/following::td[2]"))
							.getText();
					System.out.println("Sub Project Type" + getsubprojecttype);
					Assert.assertEquals(getsubprojecttype, ProjectType);
					Thread.sleep(2000);
					// Get the projectsub type
					String getprojectsubtype = driver
							.findElement(By.xpath("//p[text()='Project Subtype ']/following::td[2]")).getText();
					System.out.println("Sub Project Type" + getprojectsubtype);
					Thread.sleep(2000);
					// Get the Consutruction type
					String getprojectConsutructiontype = driver
							.findElement(By.xpath("//p[text()='Construction Type ']/following::td[2]")).getText();
					System.out.println("Sub Project Type" + getprojectConsutructiontype);
					Assert.assertEquals(getprojectConsutructiontype, ConstructionType);
					Thread.sleep(2000);
					// driver.findElement(By.id("BackToProjects")).click();
					// Thread.sleep(3000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "SubProject CardVeiw");
					System.out.println(time);
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\SubProject CardVeiw.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "SubProject CardVeiw" + "<br />" + "<b style='color:#FF0000';>Exception: </b>"
							+ e + "<br />" + test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in SubProject CardVeiw");
				test.log(LogStatus.SKIP, "SubProject CardVeiw");
			}
		}

		public void SubProjectGridView() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(19).getCell(2).getStringCellValue());
			String Input = sh1.getRow(19).getCell(2).getStringCellValue();
			String[] getsubprojectinput = Input.split(";|=");
			String SubProjectName = getsubprojectinput[1];
			String ProjectType = getsubprojectinput[3];
			String Startdate = getsubprojectinput[5];
			String Enddate = getsubprojectinput[7];
			String ConstructionType = getsubprojectinput[11];
			// Get the status row
			String TestStatus = "Yes";
			long startTime = 0;
			String Status = sh1.getRow(19).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				try {
					startTime = System.nanoTime();
					// driver.findElement(By.id("Dashboard")).click();
					Thread.sleep(2000);
					// click on the Grid icon
					driver.findElement(By.xpath("//i[@class='blm-icon-grid']")).click();
					Thread.sleep(2000);
					// mouse over to projectname
					WebElement elementactions1 = driver.findElement(By.xpath("//span[text()='Project Name']"));
					Actions act = new Actions(driver);
					act.moveToElement(elementactions1).perform();
					Thread.sleep(2000);
					// click the filter icon
					driver.findElement(By.xpath("(//div[@class='e-filtermenudiv e-icons e-icon-filter'])[1]")).click();
					Thread.sleep(2000);
					WebElement ele = driver.findElement(By.xpath("(//span[@role='listbox'])[2]"));
					ele.click();
					ele.sendKeys("Starts With");
					ele.click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@placeholder='Enter the value']")).sendKeys(SubProjectName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[text()='Filter']")).click();
					// Check the project name
					String Getprojctname = driver.findElement(By.xpath("(//span[@placement='bottom'])[1]")).getText();
					System.out.println("Project Name_" + Getprojctname);
					Assert.assertEquals(Getprojctname, SubProjectName);
					Thread.sleep(2000);
					// Get Project type
					String getprojecttype = driver.findElement(By.xpath("(//span[@placement='bottom'])[2]")).getText();
					System.out.println("Project Type_" + getprojecttype);
					Assert.assertEquals(getprojecttype, ProjectType);
					Thread.sleep(2000);
					// Get Project sub type
					String getsubprojecttype = driver.findElement(By.xpath("(//span[@placement='bottom'])[3]")).getText();
					System.out.println("Project Type_" + getsubprojecttype);
					Thread.sleep(2000);
					// Get Construction type
					String getconstructiontype = driver.findElement(By.xpath("(//span[@placement='bottom'])[4]")).getText();
					System.out.println("Construction Type_" + getconstructiontype);
					Thread.sleep(2000);
					Assert.assertEquals(getconstructiontype, ConstructionType);
					// Get the start date
					String getstartdate = driver.findElement(By.xpath("(//span[@placement='bottom'])[5]")).getText();
					System.out.println("Startdate_" + getstartdate);
					if (getstartdate.equals(Startdate)) {
						System.out.println("pass");
					} else {
						System.out.println("fail");
						long endTime = System.nanoTime();
						long totalTime = endTime - startTime;
						System.out.println(totalTime);
						long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
						long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
						System.out.println(minutes + "min" + convert + "sec");
						String time = minutes + " min, " + convert + " sec";
						System.out.println(time);
						String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\SubProject GridView Startdate.png";
						Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
						Robot robot = new Robot();
						BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
						ImageIO.write(screenFullImage, "png", new File(fileName));
						Thread.sleep(4000);
						test.log(LogStatus.FAIL, "SubProject GridView Start Date" + "<br />"
								+ "<b style='color:#FF0000';>Exception: </b>" + "<br />" + test.addScreenCapture(fileName));
					}
					Thread.sleep(2000);
					// Get the End date
					String getenddate = driver.findElement(By.xpath("(//span[@placement='bottom'])[6]")).getText();
					System.out.println("Enddate_" + getenddate);
					if (getenddate.equals(Enddate)) {
						System.out.println("pass");
					} else {
						System.out.println("fail");
						long endTime = System.nanoTime();
						long totalTime = endTime - startTime;
						System.out.println(totalTime);
						long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
						long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
						System.out.println(minutes + "min" + convert + "sec");
						String time = minutes + " min, " + convert + " sec";
						System.out.println(time);
						String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\SubProject GridView EndDate.png";
						Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
						Robot robot = new Robot();
						BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
						ImageIO.write(screenFullImage, "png", new File(fileName));
						Thread.sleep(4000);
						test.log(LogStatus.FAIL, "SubProject GridView End Date" + "<br />"
								+ "<b style='color:#FF0000';>Exception: </b>" + "<br />" + test.addScreenCapture(fileName));
					}
					Thread.sleep(2000);
					driver.findElement(By.id("BackToProjects")).click();
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "SubProject GridView");
					
				} catch (Exception e) {
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM SmokeTest\\DEV\\SubProject GridView.png";
					Rectangle rectArea = new Rectangle(1, 1, 1900, 1080);
					Robot robot = new Robot();
					BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
					ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL, "SubProject GridView" + "<br />" + "<b style='color:#FF0000';>Exception: </b>"
							+ e + "<br />" + test.addScreenCapture(fileName));
				}
			} else {
				System.out.println("Change the Status in SubProject GridView");
				test.log(LogStatus.SKIP, "SubProject GridView");
				
			}
		}
		
		public void ProjectOverview() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(13).getCell(2).getStringCellValue());
			String Input = sh1.getRow(13).getCell(2).getStringCellValue();
			String[] splits = Input.split(";|=");
			String ProjectName = splits[1];
			System.out.println(ProjectName);
			// Get the status row
			String TestStatus = "Yes";
			long startTime = 0;
			String Status = sh1.getRow(20).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				try {
					startTime = System.nanoTime();
					driver.findElement(By.id("Dashboard")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//i[@class='blm-icon-project']")).click();
					Thread.sleep(2000);
					// clicks the manage projects
					driver.findElement(By.id("ManageProjects")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("facog")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//h3[text()='Filter']//following::input[1]")).sendKeys(ProjectName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//i[@class='blm-icon-hamburger'])[3]")).click();
					Thread.sleep(3000);
					// Clicks the project info icon
					driver.findElement(By.id("icon_0")).click();
					Thread.sleep(2000);
					// clicks the image full screen icon
					driver.findElement(By.id("fullscreen")).click();
					Thread.sleep(2000);
					// Closer the full screen
					driver.findElement(By.xpath("//button[text()='X']")).click();
					Thread.sleep(2000);
					// Check the project id);
					String getprojectid = driver.findElement(By.xpath("//td[text()='Project ID']/following-sibling::td[2]"))
							.getText();
					System.out.println("Get the Project ID__" + getprojectid);
					Thread.sleep(2000);

					// Check the project version
					String getprojectversioninfo = driver
							.findElement(By.xpath("//td[text()='Version #']/following-sibling::td[2]")).getText();
					System.out.println("Get the Project Version__" + getprojectversioninfo);
					Thread.sleep(2000);

					// Check the project type
					String checkprojecttype = driver
							.findElement(By.xpath("//td[text()='Project Type']/following-sibling::td[2]")).getText();
					System.out.println("Get the Project Type" + checkprojecttype);
					Thread.sleep(2000);

					// Check the startdate
					String startdate = driver.findElement(By.xpath("//td[text()='Start Date']/following::td[2]")).getText();
					System.out.println("Get the Project Start date" + startdate);
					Thread.sleep(2000);

					// Check the enddate
					String enddate = driver.findElement(By.xpath("//td[text()='End Date']/following::td[2]")).getText();
					System.out.println("Get the Project End date" + enddate);
					Thread.sleep(2000);

					// Check the project status
					String getprojectstatus = driver.findElement(By.xpath("//td[text()='Status']/following-sibling::td[2]"))
							.getText();
					System.out.println("Get Project Status__" + getprojectstatus);
					Thread.sleep(2000);
					// Clicks the description tab
					driver.findElement(By.id("nav-profile-tab")).click();
					Thread.sleep(2000);
					String getdescriptiontext = driver.findElement(By.id("Description_rte-edit-view")).getText();
					System.out.println("Get the Project description__" + getdescriptiontext);
					Thread.sleep(2000);
					// clicks the Teams tab
					driver.findElement(By.xpath("//a[text()='Team']")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("BackToProjects")).click();
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
					test.log(LogStatus.PASS, "Project Overview");
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM\\Project Overview.png";
			        Rectangle rectArea = new Rectangle(1, 1,1900,1080);
			        Robot robot = new Robot();
			        BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
			        ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL,"Project Overview"+"<br />"+"<b style='color:#FF0000';>Exception: </b>"+e+"<br />"+ test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in Project Overview");
				test.log(LogStatus.SKIP, "Project Overview");
				
			}
		}

		public void ModelViewer() throws InterruptedException, SecurityException, IOException, AWTException {

			System.out.println(sh1.getRow(14).getCell(2).getStringCellValue());
			String Input = sh1.getRow(14).getCell(2).getStringCellValue();
			String[] splits = Input.split(";|=");
			String ProjectName = splits[1];
			System.out.println(ProjectName);
			// Get the status row
			String TestStatus = "Yes";
			long startTime = 0;
			String Status = sh1.getRow(21).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				try {
					startTime = System.nanoTime();
					driver.findElement(By.id("Dashboard")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("Project")).click();
					Thread.sleep(2000);
					// clicks the manage projects
					driver.findElement(By.id("ManageProjects")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("facog")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//h3[text()='Filter']//following::input[1]")).sendKeys(ProjectName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//i[@class='blm-icon-hamburger'])[3]")).click();
					Thread.sleep(2000);
					// Clicks the issue icon
					driver.findElement(By.id("icon_4")).click();
					Thread.sleep(3000);
					// clicks the Project checkbox
					driver.findElement(By.xpath("(//span[@class='e-frame e-icons'])[2]")).click();
					Thread.sleep(2000);

					// click the view button
					driver.findElement(By.xpath("//button[@blmtooltip='View']")).click();
					Thread.sleep(2000);
					// click on the menu bar
					driver.findElement(By.id("3d-pulse")).click();
					Thread.sleep(4000);

					// click on the full screen button
					driver.findElement(By.id("nav-fullScreen")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("nav-fullScreen")).click();
					Thread.sleep(2000);

					// click on the section clipping
					WebElement Clip = driver.findElement(By.id("iconSectionClipping"));
					Clip.click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//li[@title='xplane']")).click();
					Thread.sleep(2000);
					Clip.click();
					Thread.sleep(2000);
					Clip.click();
					Thread.sleep(2000);

					// click on the fit to screen
					driver.findElement(By.id("nav-fitToScreen")).click();
					Thread.sleep(2000);

					// select the model in the viewer
					driver.findElement(By.id("appViewer")).click();
					System.out.println("image is clicked");

					// click on the hide by category
					driver.findElement(By.id("hideByCategory")).click();
					Thread.sleep(2000);

					// click on the unhide
					driver.findElement(By.id("unHide")).click();
					Thread.sleep(2000);

					// click on the menu bar
					driver.findElement(By.id("3d-pulse")).click();
					Thread.sleep(2000);

					// Select the Element
					driver.findElement(By.id("appViewer")).click();
					Thread.sleep(4000);

					// click on the property window
					driver.findElement(By.xpath("(//a[@title='Property Window'])[1]")).click();
					Thread.sleep(2000);

					// Get the Type property
					WebElement TypeProperty = driver.findElement(By.xpath("//table[@class='table table-sm']"));
					System.out.println("-----------------------Type Properties-----------------------");
					System.out.println(TypeProperty.getText());
					Thread.sleep(2000);

					// Get the Instance property
					driver.findElement(By.id("nav-instance-tab")).click();
					Thread.sleep(2000);
					WebElement InstanceProperty = driver.findElement(By.xpath("//div[@id='collapseOne0']"));
					System.out.println("-----------------------Instance Properties-----------------------");
					System.out.println(InstanceProperty.getText());
					Thread.sleep(2000);
					System.out.println("-----------------------------------------------------------------");
					Thread.sleep(4000);

					System.out.println(
							"----------------------------------Model Viewer Passed----------------------------------");
					driver.findElement(By.id("BackToProjects")).click();
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "Model Viewer");
				
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM\\Model Viewer.png";
			        Rectangle rectArea = new Rectangle(1, 1,1900,1080);
			        Robot robot = new Robot();
			        BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
			        ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL,"Model Viewer"+"<br />"+"<b style='color:#FF0000';>Exception: </b>"+e+"<br />"+ test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in Model Viewer");
				test.log(LogStatus.SKIP, "Model Viewer");
				
			}
		}

		public void Grouping() throws Exception {
			
			if (sh1.getRow(22).getCell(4).getStringCellValue().equals("Yes")) {
				long startTime = 0;
				try {
					startTime = System.nanoTime();
					String Getprojectname = sh1.getRow(14).getCell(2).getStringCellValue();
					String[] getsubprojectinput = Getprojectname.split(";|=");
					String ProjectName = getsubprojectinput[1];
					driver.findElement(By.id("Dashboard")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("Project")).click();
					Thread.sleep(2000);
					// clicks the manage projects
					driver.findElement(By.id("ManageProjects")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("facog")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//h3[text()='Filter']//following::input[1]")).sendKeys(ProjectName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//i[@class='blm-icon-hamburger'])[3]")).click();
					Thread.sleep(2000);

					// Clicks the Model View icon
					driver.findElement(By.id("icon_4")).click();
					Thread.sleep(3000);
					// clicks the Project checkbox
					driver.findElement(By.xpath("(//span[@class='e-frame e-icons'])[2]")).click();
					Thread.sleep(2000);

					// click the view button
					driver.findElement(By.xpath("//button[@blmtooltip='View']")).click();
					Thread.sleep(4000);
					// Click on the Grouping icon
					driver.findElement(By.xpath("//a[@id='navGrouping']")).click();
					Thread.sleep(4000);

					// Get the Table data
					WebElement Group = driver.findElement(By.xpath("(//table[@class='e-table'])[2]"));
					System.out.println(Group.getText());

					// Select the Walls under Group
					WebElement ele004 = driver.findElement(By.xpath("//span[text()='Walls']"));
					Actions act1 = new Actions(driver);
					act1.moveToElement(ele004).perform();
					Thread.sleep(2000);

					// Hide the Wall under Group
					driver.findElement(By.xpath("(//i[@blmtooltip='Hide/Show'])[2]")).click();
					Thread.sleep(2000);

					// Show the Wall under Group
					driver.findElement(By.xpath("(//i[@blmtooltip='Hide/Show'])[2]")).click();
					Thread.sleep(2000);

					// Select the colour to the Group
					driver.findElement(By.xpath("(//span[@class='e-btn-icon e-icons e-caret'])[2]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[@title='Switch Mode']")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//span[@aria-label='#b71c1cff']")).click();
					Thread.sleep(2000);

					// Apply the colour to the Group
					driver.findElement(By.xpath("//button[@title='Apply']")).click();
					Thread.sleep(4000);
					driver.findElement(By.id("BackToProjects")).click();
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "Grouping");
				

				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM\\Grouping.png";
			        Rectangle rectArea = new Rectangle(1, 1,1900,1080);
			        Robot robot = new Robot();
			        BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
			        ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL,"Grouping"+"<br />"+"<b style='color:#FF0000';>Exception: </b>"+e+"<br />"+ test.addScreenCapture(fileName));
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					Thread.sleep(3000);
					
				}
			} else {
				System.out.println("Change the Status in suite file in CloudBLM_Grouping");
				test.log(LogStatus.SKIP, "Grouping");
				
			}

		}

		public void Createissues_Standalone() throws Exception {
			
			if (sh1.getRow(23).getCell(4).getStringCellValue().equals("Yes")) {
				long startTime = 0;
				try {
					startTime = System.nanoTime();
					String Getprojectname = sh1.getRow(14).getCell(2).getStringCellValue();
					String[] getsubprojectinput = Getprojectname.split(";|=");
					String ProjectName = getsubprojectinput[1];
					String Getissuename = sh1.getRow(23).getCell(2).getStringCellValue();
					String[] getissuenamevalue = Getissuename.split(";|=");
					String Standalneissuename = getissuenamevalue[1];
					String StandaloneAsigneename = getissuenamevalue[3];
					String Standaloneseverity = getissuenamevalue[5];
					String standalonestatus = getissuenamevalue[7];
					String stanalonestartdate = getissuenamevalue[9];
					String stanaloneduedate = getissuenamevalue[11];
					String Stanalonedescription = getissuenamevalue[13];
					driver.findElement(By.id("Dashboard")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("Project")).click();
					Thread.sleep(2000);
					// clicks the manage projects
					driver.findElement(By.id("ManageProjects")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("facog")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//h3[text()='Filter']//following::input[1]")).sendKeys(ProjectName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//i[@class='blm-icon-hamburger'])[3]")).click();
					Thread.sleep(2000);

					// Clicks the Model View icon
					driver.findElement(By.id("icon_4")).click();
					Thread.sleep(3000);
					// clicks the Project checkbox
					driver.findElement(By.xpath("(//span[@class='e-frame e-icons'])[2]")).click();
					Thread.sleep(2000);
					// click the view button
					driver.findElement(By.xpath("//button[@blmtooltip='View']")).click();
					Thread.sleep(4000);
					// clicks the issue icon
					driver.findElement(By.xpath("//li[@blmtooltip='Manage Issue']")).click();
					Thread.sleep(2000);
					// Click the issue icon to create the issue
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);
					// enter the issue name
					driver.findElement(By.xpath("//*[@placeholder='Issue Name']")).sendKeys(Standalneissuename);
					Thread.sleep(2000);
					// Select the assign name
					driver.findElement(By.xpath("//*[@formcontrolname='AssignedTo']")).sendKeys(StandaloneAsigneename);

					Thread.sleep(2000);
					// Select the Severity
					driver.findElement(By.xpath("//*[@formcontrolname='IssueSeverity']")).sendKeys(Standaloneseverity);
					Thread.sleep(2000);
					// select the status
					driver.findElement(By.xpath("//*[@formcontrolname='IssueStatus']")).sendKeys(standalonestatus);
					Thread.sleep(2000);
					// select the start date end date
					driver.findElement(By.xpath("(//*[@class='e-input e-lib e-keyboard'])[1]"))
							.sendKeys(stanalonestartdate);
					Thread.sleep(2000);
					driver.findElement(By.id("DueDate_input")).sendKeys(stanaloneduedate);
					Thread.sleep(2000);

					// Enter the Description
					driver.findElement(By.id("description")).sendKeys(Stanalonedescription);
					Thread.sleep(2000);

					// clicks the cancel button
					// driver.findElement(By.xpath("//button[@blmtooltip='Cancel']")).click();
					Thread.sleep(2000);
					// Cliks the create button
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("BackToProjects")).click();
					Thread.sleep(2000);

					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);

					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
					test.log(LogStatus.PASS, "Createissues_Standalone");
					

				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM\\Createissues_Standalone.png";
			        Rectangle rectArea = new Rectangle(1, 1,1900,1080);
			        Robot robot = new Robot();
			        BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
			        ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL,"Createissues_Standalone"+"<br />"+"<b style='color:#FF0000';>Exception: </b>"+e+"<br />"+ test.addScreenCapture(fileName));
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
				}
			} else {
				System.out.println("Change the Status in suite file in CloudBLM_Createissues_Standalone");
				test.log(LogStatus.SKIP, "Createissues_Standalone");
				
			}

		}

		public void Createissues_PropertyBased() throws Exception {
			if (sh1.getRow(24).getCell(4).getStringCellValue().equals("Yes")) {

				long startTime = 0;

				try {
					startTime = System.nanoTime();
					String Getprojectname = sh1.getRow(14).getCell(2).getStringCellValue();
					String[] getsubprojectinput = Getprojectname.split(";|=");
					String ProjectName = getsubprojectinput[1];
					String Getissuename = sh1.getRow(24).getCell(2).getStringCellValue();
					String[] getissuenamevalue = Getissuename.split(";|=");
					String Propertyissuename = getissuenamevalue[1];
					String PropertyAsigneename = getissuenamevalue[3];
					String Propertyseverity = getissuenamevalue[5];
					String Propertystatus = getissuenamevalue[7];
					String Propertystartdate = getissuenamevalue[9];
					String Propertyduedate = getissuenamevalue[11];
					String Propertydescription = getissuenamevalue[13];
					driver.findElement(By.id("Dashboard")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("Project")).click();
					Thread.sleep(2000);
					// clicks the manage projects
					driver.findElement(By.id("ManageProjects")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("facog")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//h3[text()='Filter']//following::input[1]")).sendKeys(ProjectName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//i[@class='blm-icon-hamburger'])[3]")).click();
					Thread.sleep(2000);

					// Clicks the Model View icon
					driver.findElement(By.id("icon_4")).click();
					Thread.sleep(3000);
					// clicks the Project checkbox
					driver.findElement(By.xpath("(//span[@class='e-frame e-icons'])[2]")).click();
					Thread.sleep(4000);
					// click the view button
					driver.findElement(By.xpath("//button[@blmtooltip='View']")).click();
					Thread.sleep(2000);
					// clicks the issue icon // Click on the Manage issue
					driver.findElement(By.id("navIssues")).click();
					Thread.sleep(2000);

					// Select the Element
					driver.findElement(By.id("appViewer")).click();
					Thread.sleep(4000);

					// click on the property window
					driver.findElement(By.xpath("(//a[@title='Property Window'])[1]")).click();
					Thread.sleep(2000);
					// Click on the issue button on Property window
					WebElement element009 = driver.findElement(By.xpath("//i[@blmtooltip='Create']"));
					Thread.sleep(2000);
					JavascriptExecutor js = (JavascriptExecutor) driver;
					js.executeScript("arguments[0].click();", element009);
					Thread.sleep(4000);
					// Enter the issue name
					driver.findElement(By.xpath("//input[@type='text']")).sendKeys(Propertyissuename);
					Thread.sleep(2000);
					// Select the assign name
					driver.findElement(By.xpath("//*[@formcontrolname='AssignedTo']")).sendKeys(PropertyAsigneename);

					Thread.sleep(2000);
					// Select the Severity
					driver.findElement(By.xpath("//*[@formcontrolname='IssueSeverity']")).sendKeys(Propertyseverity);
					Thread.sleep(2000);
					// select the status
					driver.findElement(By.xpath("//*[@formcontrolname='IssueStatus']")).sendKeys(Propertystatus);
					Thread.sleep(2000);
					// select the start date end date
					driver.findElement(By.xpath("(//*[@class='e-input e-lib e-keyboard'])[1]")).sendKeys(Propertystartdate);
					Thread.sleep(2000);
					driver.findElement(By.id("DueDate_input")).sendKeys(Propertyduedate);
					Thread.sleep(2000);

					// Enter the Description
					driver.findElement(By.id("description")).sendKeys(Propertydescription);
					Thread.sleep(2000);

					// clicks the cancel button
					// driver.findElement(By.xpath("//button[@blmtooltip='Cancel']")).click();
					Thread.sleep(2000);
					// Cliks the create button
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("BackToProjects")).click();
					Thread.sleep(2000);

					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
					test.log(LogStatus.PASS, "Createissues_PropertyBased");
					

				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM\\Createissues_PropertyBased.png";
			        Rectangle rectArea = new Rectangle(1, 1,1900,1080);
			        Robot robot = new Robot();
			        BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
			        ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL,"Createissues_PropertyBased"+"<br />"+"<b style='color:#FF0000';>Exception: </b>"+e+"<br />"+ test.addScreenCapture(fileName));
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
				}
			} else {
				System.out.println("Change the Status in suite file in CloudBLM_Createissues_PropertyBased");
				test.log(LogStatus.SKIP, "Createissues_PropertyBased");
			
			}

		}

		public void Createissues_ElementBased() throws Exception {
			if (sh1.getRow(25).getCell(4).getStringCellValue().equals("Yes")) {

				long startTime = 0;

				try {
					startTime = System.nanoTime();
					String Getprojectname = sh1.getRow(14).getCell(2).getStringCellValue();
					String[] getsubprojectinput = Getprojectname.split(";|=");
					String ProjectName = getsubprojectinput[1];
					String Getissuename = sh1.getRow(25).getCell(2).getStringCellValue();
					String[] getissuenamevalue = Getissuename.split(";|=");
					System.out.println(getissuenamevalue);
					String Elementissuename = getissuenamevalue[1];
					String ElementAsigneename = getissuenamevalue[3];
					String Elementseverity = getissuenamevalue[5];
					String Elementstatus = getissuenamevalue[7];
					String Elementstartdate = getissuenamevalue[9];
					String Elementduedate = getissuenamevalue[11];
					String Elementdescription = getissuenamevalue[13];
					driver.findElement(By.id("Dashboard")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("Project")).click();
					Thread.sleep(2000);
					// clicks the manage projects
					driver.findElement(By.id("ManageProjects")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("facog")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//h3[text()='Filter']//following::input[1]")).sendKeys(ProjectName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//i[@class='blm-icon-hamburger'])[3]")).click();
					Thread.sleep(2000);

					// Clicks the Model icon
					driver.findElement(By.id("icon_4")).click();
					Thread.sleep(3000);
					// clicks the Project checkbox
					driver.findElement(By.xpath("(//span[@class='e-frame e-icons'])[2]")).click();
					Thread.sleep(4000);
					// click the view button
					driver.findElement(By.xpath("//button[@blmtooltip='View']")).click();
					Thread.sleep(4000);

					// Select the Element
					driver.findElement(By.id("appViewer")).click();
					Thread.sleep(4000);
					// select the issue icon
					driver.findElement(By.xpath("//a[@id='navIssues']")).click();
					Thread.sleep(4000);
					// Click the issue icon to create the issue
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);
					// Enter the issue name
					driver.findElement(By.xpath("//input[@type='text']")).sendKeys(Elementissuename);
					Thread.sleep(2000);
					// Select the assign name
					driver.findElement(By.xpath("//*[@formcontrolname='AssignedTo']")).sendKeys(ElementAsigneename);

					Thread.sleep(2000);
					// Select the Severity
					driver.findElement(By.xpath("//*[@formcontrolname='IssueSeverity']")).sendKeys(Elementseverity);
					Thread.sleep(2000);
					// select the status
					driver.findElement(By.xpath("//*[@formcontrolname='IssueStatus']")).sendKeys(Elementstatus);
					Thread.sleep(2000);

					// select the start date end date
					driver.findElement(By.xpath("(//*[@class='e-input e-lib e-keyboard'])[1]")).sendKeys(Elementstartdate);
					Thread.sleep(2000);
					driver.findElement(By.id("DueDate_input")).sendKeys(Elementduedate);
					Thread.sleep(2000);

					// Enter the Description
					driver.findElement(By.id("description")).sendKeys(Elementdescription);
					Thread.sleep(2000);

					// clicks the cancel button
					// driver.findElement(By.xpath("//button[@blmtooltip='Cancel']")).click();
					Thread.sleep(2000);
					// Cliks the create button
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("BackToProjects")).click();
					Thread.sleep(2000);
					
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
					test.log(LogStatus.PASS, "Createissues_ElementBased");
				
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM\\Createissues_ElementBased.png";
			        Rectangle rectArea = new Rectangle(1, 1,1900,1080);
			        Robot robot = new Robot();
			        BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
			        ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL,"Createissues_ElementBased"+"<br />"+"<b style='color:#FF0000';>Exception: </b>"+e+"<br />"+ test.addScreenCapture(fileName));
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
				}
			} else {
				System.out.println("Change the Status in suite file in CloudBLM_Createissues_ElementBased");
				test.log(LogStatus.SKIP, "Createissues_ElementBased");
				
			}

		}

		public void Manageissues() throws Exception {
			if (sh1.getRow(26).getCell(4).getStringCellValue().equals("Yes")) {

				long startTime = 0;

				try {
					startTime = System.nanoTime();
					String Getprojectname = sh1.getRow(14).getCell(2).getStringCellValue();
					String[] getsubprojectinput = Getprojectname.split(";|=");
					String ProjectName = getsubprojectinput[1];
					driver.findElement(By.id("Dashboard")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//i[@class='blm-icon-project']")).click();
					Thread.sleep(2000);
					// clicks the manage projects
					driver.findElement(By.id("ManageProjects")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("facog")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//h3[text()='Filter']//following::input[1]")).sendKeys(ProjectName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//i[@class='blm-icon-hamburger'])[3]")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("icon_2")).click();
					Thread.sleep(2000);
					/*
					 * // change the pagenation WebElement pagenation =
					 * driver.findElement(By.xpath("//span[@role='listbox']")); pagenation.click();
					 * Thread.sleep(2000); pagenation.sendKeys("32");
					 */
					List<WebElement> issuelist = driver.findElements(By.xpath("//h2[@class='card-item-title']"));
					System.out.println(issuelist.size());
					for (int i = 0; i < issuelist.size(); i++) {
						System.out.println("Issue Name List" + issuelist.get(i).getText());
					}
					Thread.sleep(2000);
					driver.findElement(By.id("BackToProjects")).click();
					Thread.sleep(2000);

					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);

					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "Manageissues");	
					System.out.println(time);

				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM\\Manageissues.png";
			        Rectangle rectArea = new Rectangle(1, 1,1900,1080);
			        Robot robot = new Robot();
			        BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
			        ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL,"Manageissues"+"<br />"+"<b style='color:#FF0000';>Exception: </b>"+e+"<br />"+ test.addScreenCapture(fileName));
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
				}
			} else {
				System.out.println("Change the Status in suite file in CloudBLM_Manageissues");
				test.log(LogStatus.SKIP, "Manageissues");
				
			}

		}

		public void ManageRule() throws Exception {
			if (sh1.getRow(27).getCell(4).getStringCellValue().equals("Yes")) {

				long startTime = 0;

				try {
					startTime = System.nanoTime();
					String Getprojectname = sh1.getRow(14).getCell(2).getStringCellValue();
					String[] getsubprojectinput = Getprojectname.split(";|=");
					String ProjectName = getsubprojectinput[1];

					String Getrulevalues = sh1.getRow(27).getCell(2).getStringCellValue();
					String[] getruleinput = Getrulevalues.split(";|=");
					String rulename = getruleinput[1];
					String Ruletype = getruleinput[3];
					String Severity = getruleinput[5];
					String Description = getruleinput[7];
					driver.findElement(By.id("Dashboard")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("Project")).click();
					Thread.sleep(2000);
					// clicks the manage projects
					driver.findElement(By.id("ManageProjects")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("facog")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//h3[text()='Filter']//following::input[1]")).sendKeys(ProjectName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//i[@class='blm-icon-hamburger'])[3]")).click();
					Thread.sleep(2000);

					// Clicks the rule icon
					driver.findElement(By.id("icon_1")).click();
					Thread.sleep(2000);
					// clicks the create button
					driver.findElement(By.xpath("(//button[@class='blm-btn-create'])[1]")).click();
					Thread.sleep(2000);
					// Enter the Rule name
					driver.findElement(By.id("rule_name")).sendKeys(rulename);
					Thread.sleep(2000);
					// Select the ruletype
					driver.findElement(By.id("rule_type_select")).sendKeys(Ruletype);
					// select the check box
					driver.findElement(By.xpath("(//*[text()='Enable Issue Creation'])[1]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//*[text()='Enable Issue Creation'])[1]")).click();
					Thread.sleep(2000);
					// Select the severity
					driver.findElement(By.id("rule_severity")).sendKeys(Severity);
					Thread.sleep(2000);
					// Enter the description
					driver.findElement(By.id("rule_description")).sendKeys(Description);

					Thread.sleep(2000);
					driver.findElement(By.xpath("(//button[@class='blm-btn-create'])[1]")).click();
					Thread.sleep(4000);

					// select the rule category
					WebElement Category = driver
							.findElement(By.xpath("//div[@class='e-multiselect e-input-group e-checkbox']"));
					Category.click();
					Thread.sleep(2000);
					Actions builder = new Actions(driver);
					builder.sendKeys("Walls").perform();
					Thread.sleep(3000);
					driver.findElement(By.xpath("//div[@class='e-checkbox-wrapper e-css']")).click();
					Category.click();
					Thread.sleep(2000);

					// select the property
					WebElement Property = driver.findElement(By.id("param_type"));
					Property.click();
					Thread.sleep(3000);
					Property.sendKeys("Type");
					Property.click();
					Thread.sleep(2000);

					// select the Add group
					driver.findElement(By.xpath("//span[@class='e-btn-icon e-icons e-add-icon']")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//li[text()='Add Group']")).click();
					Thread.sleep(2000);

					// search the group
					WebElement Search = driver.findElement(
							By.xpath("//span[@class='e-input-group e-control-wrapper e-ddl e-lib e-keyboard']"));
					Search.click();
					Search.sendKeys("Family Name");
					Search.click();
					Thread.sleep(4000);

					// Filter the Rule
					WebElement Filter = driver.findElement(By.xpath("(//span[@role='listbox'])[2]"));
					Thread.sleep(2000);
					Filter.sendKeys("Starts With");
					Filter.click();
					Thread.sleep(2000);

					// Enter the value
					WebElement Value = driver.findElement(By.xpath("//input[@class='e-control e-textbox e-lib e-input']"));
					Value.click();
					Value.sendKeys("Basic Walls");
					Value.click();
					Thread.sleep(2000);

					// click the create button
					driver.findElement(By.xpath("//button[@blmtooltip='Create Rule']")).click();
					Thread.sleep(5000);

					// Click the back button
					// driver.findElement(By.xpath("(//button[@blmtooltip='Back'])[1]")).click();
					Thread.sleep(2000);
		
					// click the cancel button
					// driver.findElement(By.xpath("(//button[@blmtooltip='Cancel'])[1]")).click();
					Thread.sleep(2000);
					// logger.info("Manage Rule - click the cancel button");
					System.out.println(
							"----------------------------------Manage Rule Passed----------------------------------");
					driver.findElement(By.id("BackToProjects")).click();
					Thread.sleep(2000);

					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);

					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
					test.log(LogStatus.PASS, "ManageRule");
				

				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM\\ManageRule.png";
			        Rectangle rectArea = new Rectangle(1, 1,1900,1080);
			        Robot robot = new Robot();
			        BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
			        ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL,"ManageRule"+"<br />"+"<b style='color:#FF0000';>Exception: </b>"+e+"<br />"+ test.addScreenCapture(fileName));
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
				}
			} else {
				System.out.println("Change the Status in suite file in CloudBLM_ManageRule");
				test.log(LogStatus.SKIP, "ManageRule");
			
			}

		}

		public void ContentManager() throws Exception {
			if (sh1.getRow(29).getCell(4).getStringCellValue().equals("Yes")) {

				long startTime = 0;

				try {
					startTime = System.nanoTime();
					String Getprojectname = sh1.getRow(14).getCell(2).getStringCellValue();
					String[] getsubprojectinput = Getprojectname.split(";|=");
					String ProjectName = getsubprojectinput[1];
					driver.findElement(By.id("Dashboard")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//i[@class='blm-icon-project']")).click();
					Thread.sleep(2000);
					// clicks the manage projects
					driver.findElement(By.id("ManageProjects")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("facog")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//h3[text()='Filter']//following::input[1]")).sendKeys(ProjectName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//i[@class='blm-icon-hamburger'])[3]")).click();
					Thread.sleep(2000);
					// Clicks the info icon
					driver.findElement(By.id("icon_0")).click();
					Thread.sleep(2000);
					// clicks the content manager
					driver.findElement(By.id("ContentManager")).click();
					Thread.sleep(2000);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//span[@class='blm-icon-asset-mgmt content-navbar-image']")).click();

					// List<WebElement> getassertproject =
					// driver.findElements(By.xpath("//span[@class='e-list-text']"));
					// System.out.println("Assert project and sub project name-" +
					// getassertproject.size());
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//div[@class='e-fullrow'])[1]")).click();
					// driver.findElement(By.xpath("(//div[@class='e-text-content
					// e-icon-wrapper']/following::div[2])[1]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[text()='APPLY']")).click();
					Thread.sleep(2000);
					// list out the item name
					List<WebElement> lsiitencount = driver
							.findElements(By.xpath("//img[@class='img-icon ng-star-inserted']/following::span[1]"));

					System.out.println(lsiitencount.size());
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//span[@blmtooltip='Info'])[1]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//i[@class='blm-icon-angle-double-right']")).click();
					Thread.sleep(2000);
					// clicks the module icon
					driver.findElement(By.xpath("(//span[@blmtooltip='Asset Viewer'])[1]")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//i[@class='blm-icon-fullscreen']")).click();
					Thread.sleep(2000);

					driver.findElement(By.xpath("//i[@class='blm-icon-fullscreen']")).click();
					// get the project name and check it displayed or not
					WebElement projectNamegettext = driver
							.findElement(By.xpath("//td[text()='Project Name']/following::td[1]"));

					System.out.println("Project name is dispalyed");
					System.out.println("Project Name -" + projectNamegettext.getText());

					// get the Family name and check it displayed or not
					WebElement FamilyNamegettext = driver
							.findElement(By.xpath("//td[text()='Family Name']/following::td[1]"));

					System.out.println("Family Name -" + FamilyNamegettext.getText());

					// get the Family type and check it displayed or not
					WebElement FamilyTypegettext = driver
							.findElement(By.xpath("//td[text()='Family Type']/following::td[1]"));

					System.out.println("Family Type -" + FamilyTypegettext.getText());

					// get the Created Date and check it displayed or not
					WebElement Createddategettext = driver
							.findElement(By.xpath("//td[text()='Created Date']/following::td[1]"));

					System.out.println("Created date_" + Createddategettext.getText());

					// get the category and check it displayed or not
					WebElement Categorygetext = driver.findElement(By.xpath("//td[text()='Category']/following::td[1]"));

					System.out.println("Category -" + Categorygetext.getText());

					// get the Revit version and check it displayed or not
					WebElement Revitversiongettext = driver
							.findElement(By.xpath("//td[text()='Revit Version']/following::td[1]"));

					System.out.println("Revit version is dispalyed");
					System.out.println("Revit version -" + Revitversiongettext.getText());

					// get the Asset id and check it displayed or not
					WebElement GetAssertidtext = driver.findElement(By.xpath("//td[text()='Asset ID']/following::td[1]"));

					System.out.println("Asset id -" + GetAssertidtext.getText());

					Thread.sleep(2000);
					driver.findElement(By.xpath("//i[@class='blm-icon-angle-double-right']")).click();

					Thread.sleep(2000);

					driver.findElement(By.id("BackToProjects")).click();
					System.out.println("End of the Project");
					Thread.sleep(2000);

					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
					test.log(LogStatus.PASS, "ContentManager");
					

				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM\\ContentManager.png";
			        Rectangle rectArea = new Rectangle(1, 1,1900,1080);
			        Robot robot = new Robot();
			        BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
			        ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL,"ContentManager"+"<br />"+"<b style='color:#FF0000';>Exception: </b>"+e+"<br />"+ test.addScreenCapture(fileName));
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
				}
			} else {
				System.out.println("Change the Status in suite file in CloudBLM_ContentManager");
				test.log(LogStatus.SKIP, "ContentManager");
				
			}

		}

		public void ManageRFI() throws Exception {
			if (sh1.getRow(30).getCell(4).getStringCellValue().equals("Yes")) {

				long startTime = 0;

				try {
					startTime = System.nanoTime();
					String Getprojectname = sh1.getRow(14).getCell(2).getStringCellValue();
					String[] getsubprojectinput = Getprojectname.split(";|=");
					String ProjectName = getsubprojectinput[1];
					String cellValue = sh1.getRow(30).getCell(2).getStringCellValue();
					String[] splitsrfivalues = cellValue.split(";|=");
					String RFIname = splitsrfivalues[1];
					String Status = splitsrfivalues[3];
					String Assigneename = splitsrfivalues[5];
					String Severity = splitsrfivalues[7];
					String priority = splitsrfivalues[9];
					String Duedate = splitsrfivalues[11];
					String Questions = splitsrfivalues[13];
					driver.findElement(By.id("Dashboard")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//i[@class='blm-icon-project']")).click();
					Thread.sleep(2000);
					// clicks the manage projects
					driver.findElement(By.id("ManageProjects")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("facog")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//h3[text()='Filter']//following::input[1]")).sendKeys(ProjectName);
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//i[@class='blm-icon-hamburger'])[3]")).click();
					Thread.sleep(2000);
					// Clicks the info icon
					driver.findElement(By.id("icon_0")).click();
					Thread.sleep(2000);
					// Clicks the manager icon
					driver.findElement(By.id("ManageRFI")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//li[@blmtooltip='Create']")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@placeholder='RFI Name']")).sendKeys(RFIname);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//select[@placeholder='status']")).sendKeys(Status);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//select[@formcontrolname='assignee']")).sendKeys(Assigneename);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//select[@formcontrolname='severity']")).sendKeys(Severity);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//select[@formcontrolname='priority']")).sendKeys(priority);
					Thread.sleep(2000);
					driver.findElement(By.id("DueDate_input")).sendKeys(Duedate);
					Thread.sleep(2000);
					driver.findElement(By.id("question_rte-edit-view")).sendKeys(Questions);
					Thread.sleep(2000);
					driver.findElement(By.xpath("//button[text()='CREATE ']")).click();
					Thread.sleep(2000);
					// Clicks the cancel button
					// driver.findElement(By.xpath("//button[@blmtooltip='Cancel']")).click();
					Thread.sleep(2000);
					driver.findElement(By.id("BackToProjects")).click();
					Thread.sleep(2000);

					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					test.log(LogStatus.PASS, "ManagerRFI");
					System.out.println(time);

				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM\\ManagerRFI.png";
			        Rectangle rectArea = new Rectangle(1, 1,1900,1080);
			        Robot robot = new Robot();
			        BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
			        ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL,"ManagerRFI"+"<br />"+"<b style='color:#FF0000';>Exception: </b>"+e+"<br />"+ test.addScreenCapture(fileName));
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + "sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
				}
			} else {
				System.out.println("Change the Status in suite file in CloudBLM_ManagerRFI");
				test.log(LogStatus.SKIP, "ManagerRFI");
				
			}

		}

		public void Logout() throws InterruptedException, SecurityException, IOException, AWTException {

			// Get the status row
			String TestStatus = "Yes";
			long startTime = 0;
			String Status = sh1.getRow(31).getCell(4).getStringCellValue();
			if (Status.equals(TestStatus)) {
				try {
					startTime = System.nanoTime();
					driver.findElement(By.xpath("//a[@data-toggle='dropdown']")).click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("//a[text()='Logout']")).click();
					Thread.sleep(2000);
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					Thread.sleep(3000);
					System.out.println(time);
					test.log(LogStatus.PASS, "Logout");
					
				} catch (Exception e) {
					String fileName = "W:\\Fazil\\Screenshots\\CloudBLM\\Logout.png";
			        Rectangle rectArea = new Rectangle(1, 1,1900,1080);
			        Robot robot = new Robot();
			        BufferedImage screenFullImage = robot.createScreenCapture(rectArea);
			        ImageIO.write(screenFullImage, "png", new File(fileName));
					Thread.sleep(4000);
					test.log(LogStatus.FAIL,"Logout"+"<br />"+"<b style='color:#FF0000';>Exception: </b>"+e+"<br />"+ test.addScreenCapture(fileName));
					long endTime = System.nanoTime();
					long totalTime = endTime - startTime;
					System.out.println(totalTime);
					long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
					long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
					System.out.println(minutes + "min" + convert + " sec");
					String time = minutes + " min, " + convert + " sec";
					System.out.println(time);
					
				}
			} else {
				System.out.println("Change the Status in Logout");
				test.log(LogStatus.SKIP, "Logout");
			
			}
		}

		private static void generateHtmlas(String htmlContent, String outputFile) {

			FileOutputStream fop = null;
			File file;
			try {
				file = new File(outputFile);
				fop = new FileOutputStream(file);
				// if file doesnt exists, then create it
				if (!file.exists()) {
					file.createNewFile();
				}
				// get the content in bytes
				byte[] contentInBytes = htmlContent.getBytes();
				fop.write(contentInBytes);
				fop.flush();
				fop.close();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (fop != null) {
						fop.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		public void End() throws Exception {
			System.out.println("End Test...");
			long endTime = System.nanoTime();
			long totalTime = endTime - overallstartTime;
			System.out.println(totalTime);
			long convert = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
			long p1 = convert % 60;
			long p2 = convert / 60;
			long p3 = p2 % 60;
			p2 = p2 / 60;
			// long minutes = TimeUnit.MINUTES.convert(convert, TimeUnit.SECONDS);
			// System.out.println(minutes + "min" + convert + "sec");
			System.out.println(p3 + " min, " + p1 + " sec");
			String time = p3 + " min, " + p1 + " sec";
			System.out.println(time);
			test.log(LogStatus.PASS, "Quit");
			report.endTest(test);
			report.flush();
			driver.quit();
		}
		
		public static void SendMail() throws Exception {
			{
				System.out.println(sh2.getRow(1).getCell(3).getStringCellValue());
				String Email_1 = sh2.getRow(1).getCell(3).getStringCellValue();
				System.out.println(sh2.getRow(2).getCell(3).getStringCellValue());
				String Email_2 = sh2.getRow(2).getCell(3).getStringCellValue();
				System.out.println(sh2.getRow(3).getCell(3).getStringCellValue());
				String Email_3 = sh2.getRow(3).getCell(3).getStringCellValue();
				System.out.println(sh2.getRow(4).getCell(3).getStringCellValue());
				String Email_4 = sh2.getRow(4).getCell(3).getStringCellValue();
				// Email data
				String Email_Id = "fazil@srinsofttech.com";
				// change to your email ID
				String password = "fazilsst@123";
				// change to your password
				// String recipient_mail_id = "arunkumar@srinsofttech.com";
				// change to recipient email id
				String mail_subject = "CloudBLM Smoke Test Result - "+Build+" Environment";

				// Set mail properties
				Properties props = System.getProperties();
				String host_name = "smtp.gmail.com";
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", host_name);
				props.put("mail.smtp.user", Email_Id);
				props.put("mail.smtp.password", password);
				props.put("mail.smtp.port", "587");
				props.put("mail.smtp.auth", "true");

				Session session = Session.getDefaultInstance(props);
				MimeMessage message = new MimeMessage(session);

				// Set email data
				message.setFrom(new InternetAddress(Email_Id));
				message.addRecipients(Message.RecipientType.TO, Email_1);
			    message.addRecipients(Message.RecipientType.CC, Email_2);
				message.addRecipients(Message.RecipientType.CC, Email_3);
				message.addRecipients(Message.RecipientType.CC, Email_4);
				message.setSubject(mail_subject);

				MimeMultipart multipart = new MimeMultipart();
				BodyPart messageBodyPart = new MimeBodyPart();
				// Create another object to add another content
				MimeBodyPart messageBodyPart2 = new MimeBodyPart();
				// Mention the file which you want to send
				String filename = "W:\\Fazil\\CloubBLM Suite\\CloudBLM Smoke Test Report "+Build+".html";

				// Create data source and pass the filename
				DataSource source = new FileDataSource(filename);

				// set the handler
				messageBodyPart2.setDataHandler(new DataHandler(source));
			
				// set the file
				messageBodyPart2.setFileName(filename);
			
				// Create object of MimeMultipart class
				// add body part 1
				multipart.addBodyPart(messageBodyPart2);

				// set the content
				message.setContent(multipart);

				// Set key values

				// HTML mail content
				String htmlText = readEmailFromHtml("W:\\Fazil\\CloubBLM Suite\\CloudBLM Smoke Email Template.html");

				messageBodyPart.setContent(htmlText, "text/html");

				multipart.addBodyPart(messageBodyPart);
				message.setContent(multipart);

				// Conect to smtp server and send Email
				Transport transport = session.getTransport("smtp");
				transport.connect(host_name, Email_Id, password);
				transport.sendMessage(message, message.getAllRecipients());
				transport.close();
				System.out.println("message send succesfully");
			}}

			// Method to replace the values for keys
			protected static String readEmailFromHtml(String filePath) throws IOException {
				String msg = readContentFromFile(filePath);
				return msg;

			}

			// Method to read HTML file as a String
			// Method to read HTML file as a String
			private static String readContentFromFile(String fileName) throws IOException {
				StringBuffer contents = new StringBuffer();

				BufferedReader reader = new BufferedReader(new FileReader(fileName));

				String line;
				while ((line = reader.readLine()) != null) {
					contents.append(line);
					contents.append(System.getProperty("line.separator"));
				}

				{
					reader.close();
				}

				return contents.toString();
			}
		}
		